//
//  PutApi.swift
//  hris-storyboard-ios
//
//  Created by Joshua on 1/18/24.
//

import Foundation
import Alamofire

class PutApi: ParentApi {
    static let shared = PutApi()

    //cancel offset
    public func cancelOffset(userId: String, completion: @escaping (GenericResponse?, ErrorModel?) -> ()) {
        
        let endpoint = ApiEndpoints.Put.cancelOffset.replacingOccurrences(of: ApiEndpoints.Replacements.Id, with: userId)
        
        guard let url = getUrl(endpoint: endpoint) else {
            return
        }
        
        self.executeRequest(method: .put, url: url, params: nil, headers: getBasicHeader()) { (result: GenericResponse?, error) in
            if let success = result {
                completion(success, nil)
            } else {
                completion(nil, error)
            }
        }
    }
    
}

