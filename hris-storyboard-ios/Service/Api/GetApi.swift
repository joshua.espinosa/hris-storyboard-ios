//
//  GetApi.swift
//  hris-storyboard-ios
//
//  Created by Joshua on 1/18/24.
//

import Foundation
import Alamofire

class GetApi: ParentApi {
    static let shared = GetApi()
    
    private let locationBody = "location"
    private let userIdBody = "user_id"
    private let userId = "id"
    
    enum LoginError: Error {
        case invalidURL
        case apiError(LoginErrorModel)
        case networkError(Error)
        case decodingError(Error)
        case unknownError
    }
    
    //time in
    public func timeIn(location: String, userId: Int, completion: @escaping (GenericResponse?, ErrorModel?) -> ()) {
        guard let url = getUrl(endpoint: ApiEndpoints.Get.timeIn) else {
            return
        }
        
        let params : Parameters = [locationBody : location,
                                   userIdBody : userId]
        
        self.executeRequest(method: .get, url: url, params: params, headers: getBasicHeader()) { (result: GenericResponse?, error) in
            if let success = result {
                completion(success, nil)
            } else {
                completion(nil, error)
            }
        }
    }
    
    //time out
    public func timeOut(location: String, userId: Int, completion: @escaping (GenericResponse?, ErrorModel?) -> ()) {
        guard let url = getUrl(endpoint: ApiEndpoints.Get.timeOut) else {
            return
        }
        
        let params : Parameters = [locationBody : location,
                                   userIdBody : userId]
        
        self.executeRequest(method: .get, url: url, params: params, headers: getBasicHeader()) { (result: GenericResponse?, error) in
            if let success = result {
                completion(success, nil)
            } else {
                completion(nil, error)
            }
        }
    }
    
//    public func getProfileAPI(userId: Int, completion: @escaping (ProfileModel?, LoginError?) -> Void) {
//        let endpoint = ApiEndpoints.Get.getProfile.replacingOccurrences(of: ApiEndpoints.Replacements.Id, with: "\(userId)")
//        guard let url = getUrl(endpoint: endpoint) 
//        else {
//            completion(nil, LoginError.invalidURL)
//            return
//        }
//    }
//    private func runProfileStatusApi(url: URL, completion: @escaping (profileLists?, LoginError?) -> Void) {
//        executeRequest(method: .get, url: url, params: nil, headers: getBasicHeader()) {
//        (result: ProfileModel?, error) in
//            if let result = result {
//                completion(result, nil)
//                print("API Success")
//            } else {
//                completion(nil, LoginError.invalidURL)
//                print("API Failed")
//            }
//        }
//    }
  
    //get new profile API
    //Rollback Procedure
//    public func getProfileAPI(userID: Int, completion: @escaping(Result<ProfileModel?, LoginError>) -> Void){
//        let baseUrl = getUrl(endpoint: ApiEndpoints.Get.getProfile) //Replace with your actual base URL
//        
//        guard var urlComponents = URLComponents(url: baseUrl!, resolvingAgainstBaseURL: false) else {
//            return
//        }
//        urlComponents.path += "/\(userID)"
//        
//        guard let apiUrl = urlComponents.url else {
//            completion(.failure(.invalidURL))
//            return
//        }
//        
//        let request = AF.request(apiUrl, method: .get, headers: getBasicHeader())
//        
//        request.responseDecodable(of: ProfileModel.self) { response in
//            switch response.result {
//            case .success(let profileModel):
//                completion(.success(profileModel))
//            case .failure(let error):
//                if let data = response.data {
//                    if let loginError = try? JSONDecoder().decode(LoginErrorModel.self, from: data) {
//                        completion(.failure(.apiError(loginError)))
//                        
//                    } else {
//                        completion(.failure(.networkError(error)))
//                    }
//                } else {
//                    completion(.failure(.networkError(error)))
//                }
//            }
//        }
//    }
    
//
//    public func getProfileAPI(userID: Int, completion: @escaping(Result<ProfileModel?, LoginError>) -> Void) {
//        // Assuming getUrl(endpoint:) returns the base URL with {id} placeholder
//        guard var profileUrlString = getUrl(endpoint: ApiEndpoints.Get.getProfile)?.absoluteString else {
//            completion(.failure(.invalidURL))
//            return
//        }
//        // Replace the {id} placeholder with the actual user ID
//        let replacedUrl = profileUrlString.replacingOccurrences(of: "{id}", with: "\(userID)")
//        // Convert the replaced URL string back to URL
//        guard let profileUrl = URL(string: replacedUrl) else {
//            completion(.failure(.invalidURL))
//            return
//        }
//        let request = AF.request(profileUrl, method: .get, headers: getBasicHeader())
//        request.responseDecodable(of: ProfileModel.self) { response in
//            switch response.result {
//            case .success(let profileModel):
//                completion(.success(profileModel))
//            case .failure(let error):
//                if let data = response.data,
//                   let loginError = try? JSONDecoder().decode(LoginErrorModel.self, from: data) {
//                    completion(.failure(.apiError(loginError)))
//                } else {
//                    completion(.failure(.networkError(error)))
//                }
//            }
//        }
//    }
//    
    
    //get profiledetails
    public func profileAPI(userId: String, completion: @escaping(Result<ProfileModel?, LoginError>) -> Void) {
        let url = getUrl(endpoint: ApiEndpoints.Get.getProfile.replacingOccurrences(of: ApiEndpoints.Replacements.Id, with: userId))
        
        let request = AF.request(url!,
                                method: .get,
                                parameters: nil,
                                encoding: JSONEncoding.default,
                                headers: getBasicHeader())
        
        request.responseDecodable(of: ProfileModel.self) { response in
            switch response.result {
            case.success(let loginDetails):
                completion(.success(loginDetails))
            case .failure(let error):
                if let data = response.data,
                             let loginError = try? JSONDecoder().decode(LoginErrorModel.self, from: data) {
                              completion(.failure(.apiError(loginError)))
                          } else {
                              completion(.failure(.networkError(error)))
                          }
            }
        }
    }
    //get Notification Trial
    public func notificationsAPI(userId: String, dateFrom: String, dateTo: String, completion: @escaping ([notifLists]?, ErrorModel?) -> () ) {
        let endpoint = ApiEndpoints.Get.getUserNotification.replacingOccurrences(of: ApiEndpoints.Replacements.Id, with: userId).replacingOccurrences(of: ApiEndpoints.Replacements.DateFrom, with: dateFrom).replacingOccurrences(of: ApiEndpoints.Replacements.DateTo, with: dateTo)
        
        self.runNotifStatusApi(endpoint: endpoint) { response, error in
            completion(response, error)
            
        }
        
    }
    
    private func runNotifStatusApi(endpoint: String, completion: @escaping ([notifLists]?, ErrorModel?) -> ()) {
        guard let url = getUrl(endpoint: endpoint) else {
            return
        }
        self.executeRequest(method: .get, url: url, params: nil, headers: getNotifHeader()) {
            (result: NotificationsModel?, error) in
            
            if let success = result {
                completion(success.notifList, nil)
            } else {
                completion(nil, error)
            }
        }
    }

    
    
    //get notifications
//    public func notificationsAPI(userId: String, dateFrom: String, dateTo: String, completion: @escaping(Result<NotificationsModel, LoginError>) -> Void) {
//        let url = getUrl(endpoint: ApiEndpoints.Get.getUserNotification.replacingOccurrences(of: ApiEndpoints.Replacements.Id, with: userId).replacingOccurrences(of: ApiEndpoints.Replacements.DateFrom, with: dateFrom).replacingOccurrences(of: ApiEndpoints.Replacements.DateTo, with: dateTo))
//        
//        let request = AF.request(url!,
//                                 method: .get,
//                                 parameters: nil,
//                                 encoding: JSONEncoding.default,
//                                 headers: getNotifHeader())
//        
//        
//        request.responseDecodable(of: NotificationsModel.self ) { response in
//            print("Requestssss\(response)")
//            switch response.result {
//            case.success(let notifications):
//                completion(.success(notifications))
//            case .failure(let error):
//                if let data = response.data,
//                             let loginError = try? JSONDecoder().decode(LoginErrorModel.self, from: data) {
//                              completion(.failure(.apiError(loginError)))
//                          } else {
//                              completion(.failure(.networkError(error)))
//                          }
//        
//            }
//        }
//    }

    
    
//    public func getProfileDetails(userId: String, completion: @escaping (ProfileModel?, ErrorModel?) -> ()) {
//        
//        let endpoint = ApiEndpoints.Get.getProfile.replacingOccurrences(of: ApiEndpoints.Replacements.Id, with: userId)
//        
//        guard let url = getUrl(endpoint: endpoint) else {
//            return
//        }
//        
//        self.executeRequest(method: .get, url: url, params: nil, headers: getTokenHeader()) { (result: ProfileModel?, error) in
//            if let success = result {
//                completion(success, nil)
//                print("got data")
//            } else {
//                completion(nil, error)
//                print("no data")
//            }
//        }
//    }
    
    //get overtime list
    public func getOvertimeList(userId: String, dateFrom: String, dateTo: String, completion: @escaping (GenericResponse?, ErrorModel?) -> ()) {
        
        let endpoint = ApiEndpoints.Get.getOTFilteredUser.replacingOccurrences(of: ApiEndpoints.Replacements.Id, with: userId).replacingOccurrences(of: ApiEndpoints.Replacements.DateFrom, with: dateFrom)
            .replacingOccurrences(of: ApiEndpoints.Replacements.DateTo, with: dateTo)
        
        guard let url = getUrl(endpoint: endpoint) else {
            return
        }
        
        self.executeRequest(method: .get, url: url, params: nil, headers: getBasicHeader()) { (result: GenericResponse?, error) in
            if let success = result {
                completion(success, nil)
            } else {
                completion(nil, error)
            }
        }
    }
    
    //get offset list
    public func getOffsetList(userId: String, dateFrom: String, dateTo: String, completion: @escaping (GenericResponse?, ErrorModel?) -> ()) {
        
        let endpoint = ApiEndpoints.Get.getAllOffsetFilteredUser.replacingOccurrences(of: ApiEndpoints.Replacements.Id, with: userId).replacingOccurrences(of: ApiEndpoints.Replacements.DateFrom, with: dateFrom)
            .replacingOccurrences(of: ApiEndpoints.Replacements.DateTo, with: dateTo)
        
        guard let url = getUrl(endpoint: endpoint) else {
            return
        }
        
        self.executeRequest(method: .get, url: url, params: nil, headers: getBasicHeader()) { (result: GenericResponse?, error) in
            if let success = result {
                completion(success, nil)
            } else {
                completion(nil, error)
            }
        }
    }
   
    //get leave list
    public func getLeaveList(userId: String, dateFrom: String, dateTo: String, completion: @escaping (GenericResponse?, ErrorModel?) -> ()) {
        
        let endpoint = ApiEndpoints.Get.getAllLeaveFilteredUser.replacingOccurrences(of: ApiEndpoints.Replacements.Id, with: userId).replacingOccurrences(of: ApiEndpoints.Replacements.DateFrom, with: dateFrom)
            .replacingOccurrences(of: ApiEndpoints.Replacements.DateTo, with: dateTo)
        
        guard let url = getUrl(endpoint: endpoint) else {
            return
        }
        
        self.executeRequest(method: .get, url: url, params: nil, headers: getBasicHeader()) { (result: GenericResponse?, error) in
            if let success = result {
                completion(success, nil)
            } else {
                completion(nil, error)
            }
        }
    }
    
    //get user via designation
    public func getUserByDesignation(title: String, completion: @escaping (GenericResponse?, ErrorModel?) -> ()) {
        
        let endpoint = ApiEndpoints.Get.getLeaveByUserDesignation.replacingOccurrences(of: ApiEndpoints.Replacements.Title, with: title)
        
        guard let url = getUrl(endpoint: endpoint) else {
            return
        }
        
        self.executeRequest(method: .get, url: url, params: nil, headers: getBasicHeader()) { (result: GenericResponse?, error) in
            if let success = result {
                completion(success, nil)
            } else {
                completion(nil, error)
            }
        }
    }
}

