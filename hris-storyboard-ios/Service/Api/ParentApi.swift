//
//  ParentApi.swift
//  hris-storyboard-ios
//
//  Created by Joshua on 1/3/24.
//

import Foundation
import Alamofire

/// Class that can be inherited by other API services
/// This will hold the generic methods by the API services
public class ParentApi {
    
    public func getUrl(endpoint: String) -> URL? {
        guard let url = URL(string: getApi(endpoint: endpoint)) else {
            return nil
        }
        
        return url
    }
    
    public func getNewUrl(endpoint: String) -> URL? {
        guard let url = URL(string: getNewApi(endpoint: endpoint)) else {
            return nil
        }
        return url
    }
    
    private func getApi(endpoint: String) -> String {
       return "\(ApiEndpoints.Host.host)\(endpoint)"
    }
    
    private func getNewApi(endpoint: String) -> String {
       return "\(ApiEndpoints.Host.host)\(endpoint)"
    }
    
    func parseErrorModel(response: Data) -> ErrorModel? {
        let errorModel = try? self.newJSONDecoder().decode(ErrorModel.self, from: response)
        return errorModel
    }
    
    public func getJsonData(source: String?) -> Data? {
        if let resp = source, let jsonData = resp.data(using: .utf8) {
            return jsonData
        } else {
            return nil
        }
    }
    
    public func getBasicHeader() -> HTTPHeaders {
        let headers: HTTPHeaders = [
            "Content-Type":"application/json",
            
            "Authorization":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdXRoX2RhdGEiOnsiaWQiOjExLCJzdGFmZl9udW1iZXIiOjExLCJmaXJzdG5hbWUiOiJ0cmlhbCIsIm1pZGRsZW5hbWUiOiJ0cmlhbHMiLCJsYXN0bmFtZSI6InRyaWFsIHRyaWFsc3NzIiwic3VmZml4IjpudWxsLCJuYW1lIjoidHJpYWwgdHJpYWxzc3MiLCJnZW5kZXIiOiJGZW1hbGUiLCJkb2IiOiIyMDAwLTA1LTI1IiwibW9iaWxlX251bWJlciI6IjA5MTIzNTQzMjQxIiwiY2l2aWxfc3RhdHVzIjoiU2luZ2xlIiwiYWRkcmVzcyI6Ik1ha2F0aSIsInNzcyI6IjA5MTIzLTEyMzEyIiwicGhpbGhlYWx0aCI6IjA5LTEyMzE5MTIiLCJwYWdfaWJpZyI6IjA4LTEyMzEyMjMiLCJ0YXhfaWQiOiIwOS0xMjMxMjMiLCJ1c2VyX3JvbGUiOiJFbXBsb3llZSIsImRlc2lnbmF0aW9uIjoiV2ViIERldmVsb3BlciIsImVtcGxveW1lbnRfdHlwZSI6IlJlZ3VsYXIiLCJoaXJlZF9kYXRlIjpudWxsLCJzdGF0dXMiOiJBY3RpdmUiLCJyZXNpZ25lZF9kYXRlIjpudWxsLCJzZXBhcmF0aW9uX2RhdGUiOm51bGwsImRlcGFydG1lbnQiOiJJVCIsImFzX2xvYyI6Ik1ha2F0aSIsInJlZ19pcCI6IjEyMy4xMjMxIiwic2FsYXJ5IjoyODAwMCwidXNlcm5hbWUiOiJ0cmlhbFVzZXIiLCJlbWFpbF9hZGRyZXNzIjoidHJpYWxAZ21haWwuY29tIiwicmVzZXRfdG9rZW5fZXhwaXJhdGlvbiI6bnVsbCwicmVzZXRfdG9rZW4iOm51bGwsImNvbnRhY3RfcGVyc29uX25hbWUiOiJlbWVyZ2VueVRyaWFsIiwiY29udGFjdF9wZXJzb25fYWRkcmVzcyI6Ik1ha2F0aSIsImNvbnRhY3RfcGVyc29uX251bWJlciI6IjA5MTIzMTIzMjc4IiwiY29udGFjdF9wZXJzb25fZW1haWwiOm51bGwsImNvbnRhY3RfcGVyc29uX3JlbGF0aW9uc2hpcCI6InNpc3RlciIsImFnZSI6bnVsbCwibmF0aW9uYWxpdHkiOm51bGwsInN0YXR1c19hY2NvdW50IjoidW5sb2NrZWQiLCJzdGF0dXNfZGlzYWJsZWQiOiJubyIsInN0YXR1c192ZXJpZmljYXRpb24iOiJ1bnZlcmlmaWVkIiwic3RhdHVzX2Rpc2FibGVkX2V4cGlyeSI6IjI0IiwicGFzc3dvcmRfcmVzZXRfdXJsIjpudWxsLCJwYXNzd29yZF9yZXNldF9jb2RlIjpudWxsLCJwYXNzd29yZF9hdHRlbXB0X2NvdW50IjpmYWxzZSwiaW1hZ2VfdXJsIjpudWxsLCJpbWFnZV91cmxfaWQiOm51bGwsImNyZWF0ZWRfYnkiOm51bGwsImNyZWF0ZWRfZGF0ZSI6IjIwMjMtMTEtMjhUMDE6MTA6MzEuMDAwWiIsInVwZGF0ZWRfYnkiOm51bGwsInVwZGF0ZWRfZGF0ZSI6IjIwMjQtMDItMTVUMDg6NDI6MTAuMDAwWiJ9LCJpYXQiOjE3MDgwNDY3OTksImV4cCI6MTcwODEzMzE5OX0.VBlt0Ik6AexCREi3gMeZb5iz86tiOjqd1-waQo-LZR8"
        ]
        return headers
    }
    public func getNotifHeader() -> HTTPHeaders {
        let headers: HTTPHeaders = [
            "Content-Type":"application/json",
            "accept": "application/json",
            "Authorization":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdXRoX2RhdGEiOnsiaWQiOjExLCJzdGFmZl9udW1iZXIiOjExLCJmaXJzdG5hbWUiOiJ0cmlhbCIsIm1pZGRsZW5hbWUiOiJ0cmlhbHMiLCJsYXN0bmFtZSI6InRyaWFsIHRyaWFsc3NzIiwic3VmZml4IjpudWxsLCJuYW1lIjoidHJpYWwgdHJpYWxzc3MiLCJnZW5kZXIiOiJGZW1hbGUiLCJkb2IiOiIyMDAwLTA1LTI1IiwibW9iaWxlX251bWJlciI6IjA5MTIzNTQzMjQxIiwiY2l2aWxfc3RhdHVzIjoiU2luZ2xlIiwiYWRkcmVzcyI6Ik1ha2F0aSIsInNzcyI6IjA5MTIzLTEyMzEyIiwicGhpbGhlYWx0aCI6IjA5LTEyMzE5MTIiLCJwYWdfaWJpZyI6IjA4LTEyMzEyMjMiLCJ0YXhfaWQiOiIwOS0xMjMxMjMiLCJ1c2VyX3JvbGUiOiJFbXBsb3llZSIsImRlc2lnbmF0aW9uIjoiV2ViIERldmVsb3BlciIsImVtcGxveW1lbnRfdHlwZSI6IlJlZ3VsYXIiLCJoaXJlZF9kYXRlIjpudWxsLCJzdGF0dXMiOiJBY3RpdmUiLCJyZXNpZ25lZF9kYXRlIjpudWxsLCJzZXBhcmF0aW9uX2RhdGUiOm51bGwsImRlcGFydG1lbnQiOiJJVCIsImFzX2xvYyI6Ik1ha2F0aSIsInJlZ19pcCI6IjEyMy4xMjMxIiwic2FsYXJ5IjoyODAwMCwidXNlcm5hbWUiOiJ0cmlhbFVzZXIiLCJlbWFpbF9hZGRyZXNzIjoidHJpYWxAZ21haWwuY29tIiwicmVzZXRfdG9rZW5fZXhwaXJhdGlvbiI6bnVsbCwicmVzZXRfdG9rZW4iOm51bGwsImNvbnRhY3RfcGVyc29uX25hbWUiOiJlbWVyZ2VueVRyaWFsIiwiY29udGFjdF9wZXJzb25fYWRkcmVzcyI6Ik1ha2F0aSIsImNvbnRhY3RfcGVyc29uX251bWJlciI6IjA5MTIzMTIzMjc4IiwiY29udGFjdF9wZXJzb25fZW1haWwiOm51bGwsImNvbnRhY3RfcGVyc29uX3JlbGF0aW9uc2hpcCI6InNpc3RlciIsImFnZSI6bnVsbCwibmF0aW9uYWxpdHkiOm51bGwsInN0YXR1c19hY2NvdW50IjoidW5sb2NrZWQiLCJzdGF0dXNfZGlzYWJsZWQiOiJubyIsInN0YXR1c192ZXJpZmljYXRpb24iOiJ1bnZlcmlmaWVkIiwic3RhdHVzX2Rpc2FibGVkX2V4cGlyeSI6IjI0IiwicGFzc3dvcmRfcmVzZXRfdXJsIjpudWxsLCJwYXNzd29yZF9yZXNldF9jb2RlIjpudWxsLCJwYXNzd29yZF9hdHRlbXB0X2NvdW50IjpmYWxzZSwiaW1hZ2VfdXJsIjpudWxsLCJpbWFnZV91cmxfaWQiOm51bGwsImNyZWF0ZWRfYnkiOm51bGwsImNyZWF0ZWRfZGF0ZSI6IjIwMjMtMTEtMjhUMDE6MTA6MzEuMDAwWiIsInVwZGF0ZWRfYnkiOm51bGwsInVwZGF0ZWRfZGF0ZSI6IjIwMjQtMDItMTVUMDg6NDI6MTAuMDAwWiJ9LCJpYXQiOjE3MDgwNDY3OTksImV4cCI6MTcwODEzMzE5OX0.VBlt0Ik6AexCREi3gMeZb5iz86tiOjqd1-waQo-LZR8"
        ]
        return headers
    }
    
    public func getTokenJsonHeader() -> HTTPHeaders {
        let headers: HTTPHeaders = [
            Constants.Headers.Authorization: Session.shared.getBearerToken(),
            Constants.Headers.ContentType: Constants.Headers.ContentTypeJson,
            "Content-Type":"application/json",
        ]
        return headers
    }
    
    public func getTokenHeader() -> HTTPHeaders {
        let headers: HTTPHeaders = [
            Constants.Headers.Authorization: Session.shared.getBearerToken(),
            "Content-Type":"application/json"
            //"accept": "application/json"
        ]
        return headers
    }
    
    
    public func getTokenEncodedHeader() -> HTTPHeaders {
        let headers: HTTPHeaders = [
            Constants.Headers.Authorization: Session.shared.getBearerToken(),
            "Content-Type": "application/x-www-form-urlencoded",
            "Accept": "*/*"
        ]
        return headers
    }
    
    public func request(method: HTTPMethod, url: URL, params: Parameters? = nil, headers: HTTPHeaders? = nil, completion: @escaping (String?, Int?) -> ()) {
        Logger.log(message: "Requesting to: \(url.absoluteString)")
        AF.request(url, method: method, parameters: params, encoding: URLEncoding.default, headers: headers)
            .responseString { (response) in
                if let value = response.value {
                    completion(value, response.response?.statusCode)
                } else {
                    completion(nil, response.response?.statusCode)
                }
            }
    }
    
    public func requestJson(method: HTTPMethod, url: URL, requestString: String, completion: @escaping (String?, Int?) -> ()) {
        Logger.log(message: "Requesting to: \(url.absoluteString)")
        AF.request(url, method: method, parameters: [:], encoding: requestString, headers: getTokenJsonHeader())
            .responseString { (response) in
                if let value = response.value {
                    completion(value, response.response?.statusCode)
                } else {
                    completion(nil, response.response?.statusCode)
                }
            }
    }
    
    func executeRequest<T>(method: HTTPMethod, url: URL, params: Parameters? = nil, headers: HTTPHeaders? = nil, completion: @escaping (T?, ErrorModel?) -> Void) where T: Codable {
        Logger.log(message: "URL: \(url.absoluteURL)")
        AF.request(url,method: method,parameters: params,encoding: URLEncoding.default, headers: headers).responseData(completionHandler: {response in
            switch response.result{
            case .success(let res):
                if let code = response.response?.statusCode {
                    Logger.log(message: "Response: \(String(data: res, encoding: .utf8) ?? "Nothing Received")")
                    switch code {
                    case Constants.Api.StatusOk:
                        if let successModel = try? self.newJSONDecoder().decode(T.self, from: res) {
                            completion(successModel, nil)
                        } else {
                            completion(nil, nil)
                        }
                    default:
                        if let errorModel = try? self.newJSONDecoder().decode(ErrorModel.self, from: res) {
                            completion(nil, errorModel)
                        } else {
                            completion(nil, nil)
                        }
                    }
                }
            case .failure( _):
                completion(nil, nil)
            }
        })
    }
    
    func getCodableFrom<T>(string: String, completion: @escaping (T?) -> Void) where T: Codable {
        if let jsonData = getJsonData(source: string), let successModel = try? self.newJSONDecoder().decode(T.self, from: jsonData) {
            completion(successModel)
        } else {
            completion(nil)
        }
    }
    
    func checkIfExpiredToken(error: ErrorModel) -> Bool {
        if let code = error.code, code == Constants.Api.InvalidToken {
            return true
        } else {
            return false
        }
    }
    
    public func newJSONDecoder() -> JSONDecoder {
        let decoder = JSONDecoder()
        if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
            decoder.dateDecodingStrategy = .iso8601
        }
        return decoder
    }

    public func newJSONEncoder() -> JSONEncoder {
        let encoder = JSONEncoder()
        if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
            encoder.dateEncodingStrategy = .iso8601
        }
        return encoder
    }
    
    public func getJsonValue(params: Parameters) -> String? {
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: params, options: JSONSerialization.WritingOptions.prettyPrinted)
            if let JSONString = String(data: jsonData, encoding: String.Encoding.utf8) {
                return JSONString
            }
        } catch {
            return nil
        }
        
        return nil
    }
}

extension String: ParameterEncoding {
    public func encode(_ urlRequest: URLRequestConvertible, with parameters: Parameters?) throws -> URLRequest {
        var request = try urlRequest.asURLRequest()
        request.httpBody = data(using: .utf8, allowLossyConversion: false)
        return request
    }
}
