//
//  ApiEndpoints.swift
//  hris-storyboard-ios
//
//  Created by Joshua on 1/3/24.
//

import Foundation

public class ApiEndpoints {
    public class Host {
        public static var host = "http://192.168.102.51:4100/"
    }
    
    public class Post {
        //login
        public static var login = "v1/public/users/login"
        //Forgot Password
        public static var forgot = "v1/private/users/forgot-password"
        //Reset Password
        public static var reset = "v1/private/users/reset-password"
        //apply ot
        public static var addOT = "v1/private/users/ot/add-ot"
        //apply offset
        public static var addOffset = "v1/private/users/offset/add-offset"
        //apply leave (same endpoint with offset but different model"
        public static var addLeave = "v1/private/users/offset/add-offset"
    }
   
    public class Get {
        //time in & out
        public static var timeIn = "v1/private/users/trigger-time-in"
        public static var timeOut = "v1/private/users/trigger-time-out"
        
        //profile
        public static var getProfile = "v1/private/users/info/{id}"
        
        //overtime
        public static var getAllOT = "v1/private/users/ot/view-ot" // all overtime
        public static var getOTFiltered = "v1/private/users/ot/filter/{dateFrom}/{dateTo}" // yyyy-MM-dd
        public static var getOTFilteredUser = "v1/private/users/ot/filterById/{id}/{dateFrom}/{dateTo}?page=1"
        
        //offset
        public static var getAllOffset = "v1/private/users/offset/view-offset"
        public static var getAllOffsetFiltered = "v1/private/users/offset/filter/{dateFrom}/{dateTo}"
        public static var getAllOffsetFilteredUser = "v1/private/users/offset/filterById/{id}/{dateFrom}/{dateTo}?page=1"
        
        //leave
        public static var getAllLeave = "v1/private/users/leave/view-leave"
        public static var getAllLeaveFiltered = "v1/private/users/leave/filter/{dateFrom}/{dateTo}"
        public static var getAllLeaveFilteredUser = "v1/private/users/leave/filterByUserID/{id}/{dateFrom}/{dateTo}?page=1"
        public static var getLeaveByUserDesignation = "v1/private/users/leave/view-user-by-designation?title={title}"
        
        
        //notification
        //get-user-notification/:id/:dateFrom/:dateTo/'
        public static var getUserNotification = "v1/private/notifications/get-user-notification/{id}/{dateFrom}/{dateTo}"
//        public static var getUserNotificationFiltered = "v1/private/users/notifcation/filter/{dateFrom}/{dateTo}"
//        public static var getUserNotificationFilteredUser = "v1/private/users/notifcation/filterUserID{id}/{dateFrom}/{dateTo}?page=1"
//        public static var getUserNotificationDesignation = "v1/private/users/notification"
        
    }
    
    public class Put {
        public static var cancelOffset = "v1/private/users/offset/cancel-offset/{id}"
    }
 
    public class Replacements {
        public static let DateFrom = "{dateFrom}"
        public static let DateTo = "{dateTo}"
        public static let Id = "{id}"
        public static let Title = "{title}"
    }
}
