//
//  PostApi.swift
//  hris-storyboard-ios
//
//  Created by Joshua on 1/17/24.
//

import Foundation
import Alamofire

class PostApi: ParentApi {
    static let shared = PostApi()
    
    private let tokenBody = "token"
    private let emailBody = "email_address"
    private let forgotEmail = "email"
    private let passwordBody = "password"
    private let totalHourBody = "total_hour"
    private let nameBody = "name"
    private let dateBody = "date"
    private let timeFromBody = "time_from"
    private let timeToBody = "time_to"
    private let reasonBody = "reason"
    private let statusBody = "status"
    private let createdByBody = "created_by"
    private let userIdBody = "user_id"
    private let employeeIdBody = "employeeId"
    private let employeeNameBody = "emp_name"

    
    // MARK: - CustomError
    enum LoginError: Error {
        case apiError(LoginErrorModel)
        case networkError(Error)
        case decodingError(Error)
        case unknownError
    }
    
    //login
    public func login(email: String, password: String, completion: @escaping (GenericResponse?, ErrorModel?) -> ()) {
        guard let url = getUrl(endpoint: ApiEndpoints.Post.login) else {
            return
        }
        
        let params : Parameters = [emailBody : email,
                                   passwordBody : password]
        
        self.executeRequest(method: .post, url: url, params: params, headers: getBasicHeader()) { (result: GenericResponse?, error) in
            if let success = result {
                completion(success, nil)
            } else {
                completion(nil, error)
            }
        }
    }
    
    //new login
    public func loginAPI(email: String, password: String, completion: @escaping(Result<LoginDetails?, LoginError>) -> Void) {
        let url = getUrl(endpoint: ApiEndpoints.Post.login)
        let params : Parameters = [emailBody: email,
                                passwordBody: password]
        
        let request = AF.request(url!,
                                 method: .post,
                                parameters:  params,
                                 encoding: JSONEncoding.default,
                                headers: getBasicHeader())
        
        request.responseDecodable(of: LoginDetails.self) { response in
            switch response.result {
            case.success(let loginDetails):
                completion(.success(loginDetails))
            case .failure(let error):
                if let data = response.data,
                             let loginError = try? JSONDecoder().decode(LoginErrorModel.self, from: data) {
                              completion(.failure(.apiError(loginError)))
                          } else {
                              completion(.failure(.networkError(error)))
                          }
            }
        }
    }
    
    //forgot password
    public func forgotPasswordAPI(email: String, completion: @escaping(Result<ForgotResponse?, Error>) -> Void) {
        let url = getUrl(endpoint: ApiEndpoints.Post.forgot)
        let params : Parameters = [forgotEmail: email]

        let request = AF.request(url!,
                                 method: .post,
                                 parameters: params,
                                 encoding: JSONEncoding.default,
                                 headers: getBasicHeader())

        request.responseDecodable(of: ForgotResponse.self) { response in
            switch response.result {
            case.success(let forgotResponse):
                completion(.success(forgotResponse))
            case .failure(let error):
                completion(.failure(error))
            }

        }
    }
    //Reset Password
    public func resetPasswordAPI(token: String, password: String, completion: @escaping(Result<ForgotResponse?, Error>) -> Void) {
        let url = getUrl(endpoint: ApiEndpoints.Post.reset)
        let params : Parameters = [tokenBody: token,
                                passwordBody: password]
        
        let request = AF.request(url!,
                                 method:  .post,
                                parameters: params,
                                 encoding: JSONEncoding.default,
                                headers:  getBasicHeader())
         
        request.responseDecodable(of: ForgotResponse.self) { response in
            switch response.result {
            case.success(let forgotResponse):
                completion(.success(forgotResponse))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    //add overtime
    public func addOvertime(totalHour: Int, name: String, date: String, timeFrom: String, timeTo: String, reason: String, status: String, createdBy: Int, userId: Int, completion: @escaping (GenericResponse?, ErrorModel?) -> ()) {
        
        guard let url = getUrl(endpoint: ApiEndpoints.Post.addOT) else {
            return
        }
        
        let params : Parameters = [totalHourBody : totalHour,
                                        nameBody : name,
                                        dateBody : date,
                                    timeFromBody : timeFrom,
                                      timeToBody : timeTo,
                                      reasonBody : reason,
                                      statusBody : status,
                                   createdByBody : createdBy,
                                      userIdBody : userId]
        
        self.executeRequest(method: .post, url: url, params: params, headers: getTokenHeader()) { (result: GenericResponse?, error) in
            if let success = result {
                completion(success, nil)
            } else {
                completion(nil, error)
            }
        }
    }
    
    //add offset
    public func addOffset(name: String, date: String, timeFrom: String, timeTo: String, reason: String, status: String, createdBy: Int, userId: Int, completion: @escaping (GenericResponse?, ErrorModel?) -> ()) {
        
        guard let url = getUrl(endpoint: ApiEndpoints.Post.addOT) else {
            return
        }
        
        let params : Parameters = [employeeNameBody : name,
                                        dateBody : date,
                                    timeFromBody : timeFrom,
                                      timeToBody : timeTo,
                                      reasonBody : reason,
                                      statusBody : status,
                                   createdByBody : createdBy,
                                      employeeIdBody : userId]
        
        self.executeRequest(method: .post, url: url, params: params, headers: getTokenHeader()) { (result: GenericResponse?, error) in
            if let success = result {
                completion(success, nil)
            } else {
                completion(nil, error)
            }
        }
    }
    
}

