//
//  LocationDataManager.swift
//  hris-storyboard-ios
//
//  Created by Joshua on 1/9/24.
//

import Foundation
import CoreLocation
import MapKit
import SwiftUI

class LocationDataManager: NSObject, ObservableObject, CLLocationManagerDelegate {
    private let locationManager = CLLocationManager()
    @Published var location: CLLocation?

    override init() {
        super.init()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        // Request location authorization
        locationManager.requestWhenInUseAuthorization()
        // Start continuous location updates
        locationManager.startUpdatingLocation()

    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            self.location = location
        }
    }
    
    func getCoordinates() -> (CLLocationDegrees, CLLocationDegrees) {
        if let location = locationManager.location {
            return (location.coordinate.latitude, location.coordinate.longitude)
        }
        return (0, 0)
    }
    
    func getRegion() -> MKCoordinateRegion {
        return MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: getCoordinates().0, longitude: getCoordinates().1), span: MKCoordinateSpan(latitudeDelta: 0.005, longitudeDelta: 0.005))
    }
}

