//
//  Sesssion.swift
//  hris-storyboard-ios
//
//  Created by Joshua on 1/3/24.
//

import Foundation

class Session {
    public static let shared = Session()
    
    private var token = ""
    
    var resetMessage: String?
    var staffNumber: Int?
    var createdDate: String?
    
    private var profileDetail: [ProfileModel] = []


    private init() {}
    
    public func setToken(token: String) {
        self.token = token
        Logger.log(message: "Saved new access token")
    }
    
    public func removeToken() {
        self.token = ""
        Logger.log(message: "Remove access token")
    }
    
    public func getBearerToken() -> String {
        return "\(Constants.Headers.Bearer)\(self.token)"
    }
    
    public func setResetMessage(fMessage: String){
        self.resetMessage = fMessage
        Logger.log(message: "Reset Message: \(resetMessage ?? "")")
    }
    public func getResetMessage() -> String {
        return self.resetMessage ?? ""
    }
    public func setStaffNumber(idNumber: Int){
        self.staffNumber = idNumber
        Logger.log(message: "Staff Number: \(staffNumber ?? 0)")
    }
    public func getStaffNumber() -> Int {
        return self.staffNumber ?? 0
    }
    
    //Store Redeemed Response for Redemption Dialog Box
    public func getProfileDetails() -> [ProfileModel] {
        return self.profileDetail
    }
    
    public func addProfileDetails(ProfileResponse: ProfileModel) {
        self.profileDetail.append(ProfileResponse)
        print("Profile Append Response: \(ProfileResponse)")
    }
    
    public func removeProfileDetails() {
        self.profileDetail.removeAll()
        print("Profile Response: \(profileDetail.count)")
    }
    //Set Created date from profile retrieval
    public func setCreatedDate(userCreatedDate: String) {
        self.createdDate = userCreatedDate
        Logger.log(message: "User Created Date: \(createdDate ?? "")")
    }
    
    //Get Created Date from profile Retrieval
    public func getCreatedDate() -> String {
        
        return self.createdDate ?? ""
    }
    
}
