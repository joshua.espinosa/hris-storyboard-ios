//
//  ErrorPromptViewController.swift
//  hris-storyboard-ios
//
//  Created by Joshua on 1/3/24.
//

import UIKit

class ErrorPromptViewController: UIViewController {
    @IBOutlet weak var titleMessage: UILabel!
    @IBOutlet weak var messageTxt: UILabel!
    @IBOutlet weak var errorBtn: UIButton!
    
    var titleMsg: String?
    var message: String?
    var buttonText: String?
    var disableButton = false
    var action: (()->())?
    
    deinit {
        titleMsg = nil
        message = nil
        buttonText = nil
        action = nil
        Logger.log(message: "Successfully removed ErrorPromptViewController from memory")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        errorBtn.layer.cornerRadius = CGFloat(Constants.Design.ButtonCorderRadius)
        
        if let titleMsg = titleMsg {
            titleMessage.text = titleMsg
        }
        
        if let message = message {
            messageTxt.text = message
        }
        
        if let text = buttonText {
            errorBtn.setTitle(text, for: .normal)
        }
    }
    
    @IBAction func closeBtnAction(_ sender: Any) {
        if self.disableButton {
            return
        }
        
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func errorBtnAction(_ sender: Any) {
        self.dismiss(animated: false, completion: {
            if let unwrappedAction = self.action {
                unwrappedAction()
            }
        })
    }
}
