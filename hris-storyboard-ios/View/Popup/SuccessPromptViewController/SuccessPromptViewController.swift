//
//  SuccessPromptViewController.swift
//  hris-storyboard-ios
//
//  Created by Joshua on 1/3/24.
//

import UIKit

class SuccessPromptViewController: UIViewController {
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var messageLbl: UILabel!
    @IBOutlet weak var okBtn: UIButton!
    
    var headerMessage: String?
    var message: String?
    var action: (()->())?
    
    deinit {
        headerMessage = nil
        message = nil
        action = nil
        Logger.log(message: "Successfully removed SuccessPromptViewController from memory")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.okBtn.makeRoundBorder()
        
        if let header = headerMessage {
            titleLbl.text = header
        }
        
        if let message = message {
            messageLbl.text = message
        }
    }

    @IBAction func okBtnAction(_ sender: Any) {
        if let unwrappedAction = action {
            unwrappedAction()
        }
        self.dismiss(animated: false, completion: nil)
    }
}
