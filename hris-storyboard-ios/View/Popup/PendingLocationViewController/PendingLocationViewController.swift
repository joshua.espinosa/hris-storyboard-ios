//
//  PendingLocationViewController.swift
//  hris-storyboard-ios
//
//  Created by Joshua on 1/12/24.
//

import UIKit

class PendingLocationViewController: UIViewController {
    
    @IBOutlet weak var reqBtn: UIButton!
    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet var wholeView: UIView!
    var headerMessage: String?
    var message: String?
    var action: (()->())?
    
    deinit {
        action = nil
        Logger.log(message: "Successfully removed PendingLocationViewController from memory")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.reqBtn.makeRoundBorder()
        self.closeBtn.makeRoundBorder()
        
    }
    
    @IBAction func reqBtnAction(_ sender: Any) {
        if let unwrappedAction = action {
            unwrappedAction()
        }
        self.dismiss(animated: false, completion: nil)
    }

    @IBAction func closeBtnAction(_ sender: Any) {
        if let unwrappedAction = action {
            unwrappedAction()
        }
        self.dismiss(animated: false, completion: nil)
    }
}
