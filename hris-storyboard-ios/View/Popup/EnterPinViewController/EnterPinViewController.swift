//
//  EnterPinViewController.swift
//  hris-storyboard-ios
//
//  Created by Joshua on 1/12/24.
//

import UIKit

class EnterPinViewController: UIViewController {
    
    @IBOutlet weak var okBtn: UIButton!
    
    var headerMessage: String?
    var message: String?
    var action: (()->())?
    
    deinit {
        action = nil
        Logger.log(message: "Successfully removed EnterPinViewController from memory")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.okBtn.makeRoundBorder()
        
    }

    @IBAction func okBtnAction(_ sender: Any) {
        if let unwrappedAction = action {
            unwrappedAction()
        }
        self.dismiss(animated: false, completion: nil)
    }
}
