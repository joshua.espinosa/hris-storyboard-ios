//
//  ParentController.swift
//  hris-storyboard-ios
//
//  Created by Joshua on 1/3/24.
//

import Foundation
import UIKit
import SystemConfiguration
import CoreLocation
import FSCalendar

class ParentController: UIViewController, FSCalendarDelegate, FSCalendarDataSource, FSCalendarDelegateAppearance, CLLocationManagerDelegate {
    var appDelegate = UIApplication.shared.delegate as? AppDelegate
    private let charactersSet = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@#$&^.-_ ")
    private let lettersSet = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ ")
    private let numbersSet = CharacterSet(charactersIn: "1234567890")
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return self.style
    }
    
    var style: UIStatusBarStyle = .default
    
    func getDoneKeyButton(buttonName: String? = "Done") -> UIToolbar {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        doneToolbar.barStyle = .default

        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: buttonName, style: .done, target: self, action: #selector(self.doneButtonAction))

        let items = [flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        return doneToolbar
    }

    @objc func doneButtonAction(){
        self.view.endEditing(true)
    }
    
    func changeStatusBarColor(forDark: Bool) {
        self.style = (forDark) ? .default : .lightContent
        setNeedsStatusBarAppearanceUpdate()
    }
    
//    func createLocalNotification(title: String, message: String, interval: Double) {
//        self.appDelegate?.scheduleNotification(title: title, message: message, interval: interval)
//    }
//
    func showLoader() {
        ProgressHudUtil.showLoader(message: Constants.Messages.PleaseWait)
    }
    
    func hideLoader() {
        ProgressHudUtil.hideLoader()
    }
    
    
    func handleErrorAction(errorModel: ErrorModel?, action: @escaping (() -> Void)) {
        guard let error = errorModel, let code = error.code else {
            self.showErrorPrompt(message: "Server connection timeout, please click OK to try again.", buttonText: Constants.Buttons.Okay, action: nil)
            return
        }
        
    }
    
    func handleErrorResponse(errorModel: ErrorModel?) -> String {
        if let error = errorModel, let errors = error.errors {
            if let errorMessage = errors.first, let message = errorMessage.message {
                return message
            }
        }
        
        return "Something went wrong.\nPlease try again."
    }
    
    @objc func addCharacterLimit(textField: UITextField) {
        if let t: String = textField.text {
            textField.text = String(t.prefix(textField.tag))
        }
    }
    
    func createClearBackgroundView() -> UIView {
        let bgColorView = UIView()
        bgColorView.backgroundColor = UIColor.clear
        return bgColorView
    }
    
    
    func isValidCharactersOnly(source: String) -> Bool {
        //return validate(source: source, characterSet: charactersSet)
        return true
    }
    
    func isLettersOnly(source: String) -> Bool {
        return validate(source: source, characterSet: lettersSet)
    }
    
    func isNumbersOnly(source: String) -> Bool {
        return validate(source: source, characterSet: numbersSet)
    }
    
    func validate(source: String, characterSet: CharacterSet) -> Bool {
        if source.rangeOfCharacter(from: characterSet) != nil || source == "" {
            return true
        }

        return false
    }
    
    func isConnectedToInternet() -> Bool {
        Logger.log(message: "Checking for internet connection...")
        var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)

        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }

        var flags: SCNetworkReachabilityFlags = SCNetworkReachabilityFlags(rawValue: 0)
        if SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) == false {
            return false
        }

        /* Only Working for WIFI
        let isReachable = flags == .reachable
        let needsConnection = flags == .connectionRequired

        return isReachable && !needsConnection
        */

        // Working for Cellular and WIFI
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        let ret = (isReachable && !needsConnection)

        return ret
    }
    
    func showNoInternetPrompt(retryAction: @escaping(() -> Void)) {
        DispatchQueue.main.asyncAfter(deadline: .now() + Constants.Values.QuarterSecond) {
            self.showErrorPrompt(message: Constants.Messages.NoInternetConnection, buttonText: Constants.Buttons.TryAgain.uppercased(), action: {
                retryAction()
            })
        }
    }
    
    func showNoInternetPromptNoClose(retryAction: @escaping(() -> Void)) {
        DispatchQueue.main.asyncAfter(deadline: .now() + Constants.Values.QuarterSecond) {
            self.showErrorPromptNoClose(message: Constants.Messages.NoInternetConnection, buttonText: Constants.Buttons.TryAgain.uppercased(), action: {
                retryAction()
            })
        }
    }
    
    func getApproximateAdjustedFontSizeWithLabel(label: UILabel) -> CGFloat {

        if label.adjustsFontSizeToFitWidth == true {

            var currentFont: UIFont = label.font
            let originalFontSize = currentFont.pointSize
            var currentSize: CGSize = (label.text! as NSString).size(withAttributes: [NSAttributedString.Key.font: currentFont])

            while currentSize.width > label.frame.size.width && currentFont.pointSize > (originalFontSize * label.minimumScaleFactor) {
                currentFont = currentFont.withSize(currentFont.pointSize - 1)
                currentSize = (label.text! as NSString).size(withAttributes: [NSAttributedString.Key.font: currentFont])
            }

            return currentFont.pointSize

        }
        else {

            return label.font.pointSize

        }

    }
    
    func togglePasswordField(field: UITextField, button: UIButton) {
        field.isSecureTextEntry = !field.isSecureTextEntry
        button.setImage((field.isSecureTextEntry) ? #imageLiteral(resourceName: "password_visible") : #imageLiteral(resourceName: "password_hidden"), for: .normal)
    }
}

extension ParentController {
    
    func showToast(message : String, font: UIFont) {

        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 75, y: self.view.frame.size.height-100, width: 150, height: 35))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.font = font
        toastLabel.textAlignment = .center;
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
             toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    func showTicketToast(message : String, font: UIFont) {

        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 120, y: self.view.frame.size.height-100, width: 250, height: 35))
        toastLabel.numberOfLines = 0
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.font = font
        toastLabel.textAlignment = .center;
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 5.0, delay: 0.1, options: .curveEaseOut, animations: {
             toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    func showFAQToast(message : String, font: UIFont) {

        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 175, y: self.view.frame.size.height-100, width: 350, height: 35))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        toastLabel.textColor = UIColor.white
        toastLabel.font = font
        toastLabel.textAlignment = .center;
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseIn , animations: {
             toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
}

extension UIView {

  // OUTPUT 1
  func dropShadow(scale: Bool = true) {
    layer.masksToBounds = false
    layer.shadowColor = UIColor.black.cgColor
    layer.shadowOpacity = 0.5
    layer.shadowOffset = CGSize(width: -1, height: 1)
    layer.shadowRadius = 1

    layer.shadowPath = UIBezierPath(rect: bounds).cgPath
    layer.shouldRasterize = true
    layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    layer.cornerRadius = 5
  }

  // OUTPUT 2
  func dropShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true) {
    layer.masksToBounds = false
    layer.shadowColor = color.cgColor
    layer.shadowOpacity = opacity
    layer.shadowOffset = offSet
    layer.shadowRadius = radius

    layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
    layer.shouldRasterize = true
    layer.rasterizationScale = scale ? UIScreen.main.scale : 1
  }
}

