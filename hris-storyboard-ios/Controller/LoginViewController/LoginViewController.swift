//
//  LoginViewController.swift
//  hris-storyboard-ios
//
//  Created by Joshua on 1/3/24.
//

import UIKit

class LoginViewController: ParentController, UITextFieldDelegate {
    let postApi = PostApi()
    
    var debugMode = false
    var parentalGateAction = -1
    var urlToLoad: URL?
    
    var iconClick = true
    
    let URL_LOCAL = "http://192.168.50.26:4100/v1/public/users/login"
    
    @IBOutlet weak var LogInBtn: UIButton!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var emailNotValid: UILabel!
    @IBOutlet weak var eyeBtn: UIButton!
    deinit {
        Logger.log(message: "Successfully removed LoginViewController from memory")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        emailTextField.delegate = self
        emailNotValid.isHidden = true
        Logger.initializeLogger(forceDebug: true)
        
        if (Session.shared.getResetMessage().contains("Password")) == true {
            showFAQToast(message: "\(Session.shared.getResetMessage())", font: .systemFont(ofSize: 12.0))
        } else if (Session.shared.getResetMessage().contains("Invalid")) == true {
            showFAQToast(message: "\(Session.shared.getResetMessage())", font: .systemFont(ofSize: 12.0))
        } else {
            
        }
        
        LogInBtn.layer.cornerRadius = 8
        LogInBtn.makeRoundBorder()
        
        passwordTextField.isSecureTextEntry = true
  
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == emailTextField {
            let currentText = (textField.text ?? "") as NSString
            let newText = currentText.replacingCharacters(in: range, with: string)
         
            if isValidEmail(email: newText){
                emailNotValid.isHidden = true
            } else {
                emailNotValid.isHidden = false
            }
            
        }
        
        
        return true
    }
    
    @IBAction func loginBtn(_ sender: Any) {
        
//        let overLayer = FirstTimeViewController()
//        overLayer.appear(sender: self)
        
        guard let email = emailTextField.text, !email.isEmpty else {
            return
        }
        guard let password = passwordTextField.text, !password.isEmpty else {
            return
        }
        processLogin(email: email, password: password)
        
        
    }
    
    
    @IBAction func forgotPasswordAction(_ sender: Any) {
        let noticeVc = ForgotPasswordViewController()
        noticeVc.modalPresentationStyle = .overCurrentContext
        noticeVc.isModalInPresentation = true
        self.present(noticeVc, animated: false, completion: nil)
    }
    
    @IBAction func iconAction(_ sender: Any) {
        
        if iconClick {
            passwordTextField.isSecureTextEntry = false
            eyeBtn.setImage(UIImage(systemName: "eye.slash"), for: .normal)
        } else {
            passwordTextField.isSecureTextEntry = true
            eyeBtn.setImage(UIImage(systemName: "eye"), for: .normal)
        }
        iconClick = !iconClick
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    @objc func keyboardWillShow(sender: NSNotification) {
         self.view.frame.origin.y = -150 // Move view 150 points upward
    }

    @objc func keyboardWillHide(sender: NSNotification) {
         self.view.frame.origin.y = 0 // Move view to original position
    }
    
    private func processLogin(email: String, password: String) {
        
        showLoader()
        postApi.loginAPI(email: email, password: password) { result in
            self.hideLoader()
            switch result {
            case .success(let loginDetails):
                print("Success: \(loginDetails)")
                
                               Session.shared.setStaffNumber(idNumber: loginDetails?.data.id ?? 0)
                               
                               print("Login Staff Number \(Session.shared.getStaffNumber())")
                               
                               Session.shared.setToken(token: loginDetails?.data.token ?? "no token")
                               
                               Session.shared.setCreatedDate(userCreatedDate: loginDetails?.data.hiredDate ?? "")
                
//                let noticeVc = HomeViewController()
//                      noticeVc.modalPresentationStyle = .overCurrentContext
//                      noticeVc.isModalInPresentation = true
//                self.present(noticeVc, animated: false, completion: nil)

                self.dismiss(animated: true) {
                      let homeViewController = HomeViewController()
                      let navigationController = UINavigationController(rootViewController: homeViewController)
                      navigationController.modalPresentationStyle = .fullScreen  // Ensure HomeViewController is presented in full screen
                      self.present(navigationController, animated: false) {
                          // Automatically present NoticeViewController
                          homeViewController.presentNoticeViewController()
                      }
                  }
          
                
                print("")
                
            case .failure(let error):
                print("Error: \(error)")
            }
        }
    }
//    func presentNoticeViewController() {
//        let noticeViewController = FirstTimeViewController()
//        noticeViewController.modalPresentationStyle = .overCurrentContext  // Preserve underlying view controller's background
//        noticeViewController.isModalInPresentation = true
//        present(noticeViewController, animated: false, completion: nil)
//    }
    
    func isValidEmail(email: String) -> Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailPredicate = NSPredicate(format: "SELF MATCHES %@", emailRegex)
        return emailPredicate.evaluate(with: email)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    
}
extension HomeViewController {
    func presentNoticeViewController() {
        let noticeViewController = FirstTimeViewController()
        noticeViewController.modalPresentationStyle = .overCurrentContext  // Present NoticeViewController with transparent background
        present(noticeViewController, animated: false, completion: nil)
    }
}
