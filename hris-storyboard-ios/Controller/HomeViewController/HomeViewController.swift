//
//  HomeViewController.swift
//  hris-storyboard-ios
//
//  Created by Joshua on 1/8/24.
//

import UIKit
import FSCalendar
import CoreLocation
import Alamofire

class HomeViewController: ParentController {
    let getApi  = GetApi()
    
    @IBOutlet weak var notificationLabel: UILabel!
    @IBOutlet weak var profileBtn: UIImageView!
    @IBOutlet weak var notificationList: UITableView!
    @IBOutlet weak var timeButton: UIButton!
    @IBOutlet weak var currentTime: UILabel!
    @IBOutlet weak var currentDayStatus: UILabel!
    @IBOutlet weak var mycalendar: FSCalendar!
    @IBOutlet weak var notifButton: UIButton! // toggle notifications
    @IBOutlet weak var notifListButton: UIButton! //go to notification list
    @IBOutlet weak var inforCircle: UIImageView!
    
    enum LoginError: Error {
        case invalidURL
        case apiError(LoginErrorModel)
        case networkError(Error)
        case decodingError(Error)
        case unknownError
    }
    
    //STATIC DATA FOR ATTENDANCE STATUS IN CALENDAR
    var datesComplete = ["2024-01-02", "2024-01-03", "2024-01-04", "2024-01-05", "2024-01-08",
                         "2024-01-11", "2024-01-12", "2024-01-15", "2024-01-16", "2024-01-18",
                         "2024-01-23", "2024-01-24", "2024-01-25","2024-01-26", "2024-01-29", "2024-01-09"]
    var datesNoTimeOut = ["2024-01-10"]
    var datesAbsent = ["2024-01-17"]
    var datesLate = ["2024-01-22", "2024-01-19"]
    var datesLeave = ["2024-01-30", "2024-01-31"]
    
    var notifsList: [NotificationModel] = []
    var notifCell: [NotificationViewCell] = []
    var addedNotif: [NotificationModel] = []
    
    var showPopup = true

    
//    var originalCalendarYOffset: CGFloat = 0
    
    
    
    private var hasTimedIn: Bool?
    private let locationManager = CLLocationManager()
    @Published var location: CLLocation?
    
    var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
    
    deinit {
        Logger.log(message: "Successfully removed HomeViewController from memory")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
      
        firstTimeLogin()
        
     
        //initialize
        locationManager.delegate = self
        mycalendar.delegate.self
        mycalendar.dataSource.self
        notificationList.delegate = self
        notificationList.dataSource = self
//        firstTimeLogin()
        
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        // Request location authorization
        locationManager.requestWhenInUseAuthorization()
        // Start continuous location updates
        locationManager.startUpdatingLocation()
      
        
        
        mycalendar.appearance.headerTitleColor = .black
        
        //tempcheck
        hasTimedIn = false
        timeButton.backgroundColor = UIColor.systemGreen
        timeButton.makeRoundBorder()
        

        
        //profile tapped
        let TapGesture = UITapGestureRecognizer()
        self.profileBtn.addGestureRecognizer(TapGesture)
        TapGesture.addTarget(self, action: #selector(tapclick))
        
        //start currentclock
        Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { [self] (_) in
            
            let date = Date()
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "hh:mm:ss a"
            let currentTime = dateFormatter.string(from: date)
            self.currentTime.text = currentTime
            
            
        //    self.originalCalendarYOffset = self.mycalendar.frame.origin.y
        }
        
        //static notifications
        let testNotif1 = NotificationModel(date: "01/12/2024", type: "Holiday")
        let testNotif2 = NotificationModel(date: "01/13/2024", type: "Leave")
        let testNotif3 = NotificationModel(date: "01/14/2024", type: "Overtime")
        let testNotif4 = NotificationModel(date: "01/15/2024", type: "Offset")
        let testNotif5 = NotificationModel(date: "01/16/2024", type: "Payroll")
        
        notifsList.append(testNotif1)
        notifsList.append(testNotif2)
        notifsList.append(testNotif3)
        notifsList.append(testNotif4)
        notifsList.append(testNotif5)
                          
        setupNotificationCells()
//        processNotif()
    }
    
    private func firstTimeLogin() {
//        let overLayer = FirstTimeViewController()
//        overLayer.appear(sender: self)
        let noticeVc = FirstTimeViewController()
              noticeVc.modalPresentationStyle = .popover
              noticeVc.isModalInPresentation = true
              self.present(noticeVc, animated: false, completion: nil)

        print("First time login Dialog box")

    
    }
  
    
    private func processNotif() {
        showLoader()
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let currentDate = Date()
        let formattedDate = dateFormatter.string(from: currentDate)
        print(formattedDate)
        
        let userId = Session.shared.getStaffNumber()
        let fromDate = Session.shared.getCreatedDate()
        let toDate = formattedDate
        
        print("User ID: \(userId)")
        print("From Date: \(fromDate)")
        print("To Date: \(toDate)")
        
        getApi.notificationsAPI(userId: "\(userId)", dateFrom: fromDate, dateTo: toDate) {[weak self] response, error in
            self?.hideLoader()
            
            guard let notifData = response else {
                self?.handleErrorAction(errorModel: error, action: {
                    self?.processNotif()
                })
                return
            }
            
        }
        
        
//        getApi.notificationsAPI(userId: String(userId), dateFrom: fromDate, dateTo: toDate) { result in
//        
//            switch result {
//            case .success(let notifications):
//                print(result)
//                print("Success notification Response")
//                
//            case .failure(let error):
//                print("Error: \(error)")
//                print("Failed to retrieve notification Response")
//            }
//        }
    }
    
    private func setupNotificationCells() {
        
        for notif in self.notifsList {
            Logger.log(message: "Added Notification \(notif.date) + \(notif.type)")
            
            self.notifCell.append(self.createCell(notif: notif))
            self.notificationList.reloadData()
            
        }
    }
    
    
    
    //attendance status
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, fillDefaultColorFor date: Date) -> UIColor? {

     let dateString = self.dateFormatter.string(from: date)

       //color all complete dates
        // green = complete , yellow = no timeout, red = absent, pink = late, orange = onleave
        if datesComplete.contains(dateString) {
            return UIColor.systemGreen
        }
    
        if datesNoTimeOut.contains(dateString) {
            return UIColor.systemYellow
        }
        
        if datesAbsent.contains(dateString) {
            return UIColor.red
        }
    
        if datesLate.contains(dateString) {
            return UIColor.systemPink
        }
        
        if datesLeave.contains(dateString) {
            return UIColor.systemOrange
        }
    
     return nil
    }
    
    
    //profile action
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
       
        self.navigationController?.pushViewController(ProfilePageViewController(), animated: true)
      
    }
    
    //go to notificationlist view controller
    @IBAction func gotoNotificationLits(_ sender: Any) {
        self.navigationController?.pushViewController(NotificationListViewController(), animated: true)
    }
    
    //show hide notification list on homepage
    @IBAction func bellBtn(_ sender: Any) {
        // Toggle the visibility of the notification list and information circle
            notificationList.isHidden = !notificationList.isHidden
            inforCircle.isHidden = !inforCircle.isHidden
            
            // Move the calendar to the top or to its original position at the bottom based on the visibility of the notification list
            if notificationList.isHidden {
                // If the notification list is hidden, move the calendar to the top
                UIView.animate(withDuration: 0.3) {
                    self.mycalendar.frame.origin.y = 60
                }
            } else {
                // If the notification list is visible, move the calendar to its original position at the bottom
                UIView.animate(withDuration: 0.3) {
                self.mycalendar.frame.origin.y = 300
                }
            }
            
            // Show or hide the navigation bar based on the visibility of the notification list
            navigationController?.setNavigationBarHidden(notificationList.isHidden, animated: true)
    }
 
    //time in out function
    @IBAction func timeInOut(_ sender: Any) {
       
        
        if (showPopup) {
        
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5) {
                self.showUnregisteredPopup(action: {
                    self.dismiss(animated: false)
                    self.showPopup = false
                })
            }
            
        } else {
            
            if hasTimedIn == false {
                
                timeButton.backgroundColor = UIColor.systemRed
                timeButton.setTitle("Time Out", for: .normal)
                self.currentDayStatus.text = self.currentTime.text
                hasTimedIn = true
                showPopup = false
           
                
            } else {
                let timeout = self.currentTime.text ?? ""
                timeButton.backgroundColor = UIColor.systemGreen
                timeButton.setTitle("Time In", for: .normal)
                self.currentDayStatus.text?.append(" ~ " + timeout)
                hasTimedIn = false
                showPopup = false
        
            }
        }
        
     
        
    }
    
    //when notifs get clicked
    private func notificationMoreInfo(notif: NotificationModel, index: Int) {
        
        if notif.type == "Holiday" {
            Logger.log(message: "Holiday Notif Clicked")
            self.navigationController?.pushViewController(AnnouncementViewController(), animated: true)
        } else if notif.type == "Leave" {
            Logger.log(message: "Leave Notif Clicked")
            self.navigationController?.pushViewController(LeaveStatusViewController(), animated: true)
        } else if notif.type == "Overtime" {
            Logger.log(message: "Overtime Notif Clicked")
            self.navigationController?.pushViewController(OTStatusViewController(), animated: true)
        } else if notif.type == "Offset" {
            Logger.log(message: "Offset Notif Clicked")
            self.navigationController?.pushViewController(OffsetStatusViewController(), animated: true)
        } else if notif.type == "Payroll" {
            Logger.log(message: "Payroll Notif Clicked")
        
            //show pin
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5) {
                self.showPin(action: {
                    self.goToPayrollInfo()
                })
            }
            
        }
    }
    
    private func goToPayrollInfo() {
        self.navigationController?.pushViewController(PayrollStatusViewController(), animated: true)
    }
    
    @objc func tapclick(){
        let profileVc = ProfilePageViewController()
        profileVc.modalPresentationStyle = .overCurrentContext
        profileVc.isModalInPresentation = true
        self.present(profileVc, animated: false)
    }

    
   
}

extension HomeViewController:  UITableViewDelegate, UITableViewDataSource {
    
    func createCell(notif: NotificationModel) -> NotificationViewCell {
        var cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "NotificationViewCell") as? NotificationViewCell
        if cell == nil {
            cell = NotificationViewCell.createCell()
            cell?.dateLabel.text = notif.date
            cell?.typeLabel.text = notif.type
         
            
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return (UIDevice.current.userInterfaceIdiom == .pad) ? 70 : 60
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.notifsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "NotificationViewCell") as? NotificationViewCell
        cell = NotificationViewCell.createCell()
        
        if notifCell.count > indexPath.row {
            return notifCell[indexPath.row]
        }
    
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        Logger.log(message: "tap")
        if indexPath.row != self.notifsList.count {
            self.notificationMoreInfo(notif: self.notifsList[indexPath.row], index: indexPath.row)
        }
    }
}
