//
//  ChangePasswordViewController.swift
//  hris-storyboard-ios
//
//  Created by Gemmalyn on 6/18/24.
//

import UIKit

class ChangePasswordViewController: ParentController {

    
    @IBOutlet weak var oldPasstextField: UITextField!
    @IBOutlet weak var eyeBtnOldPass: UIButton!
    @IBOutlet weak var newPassTextField: UITextField!
    @IBOutlet weak var newPassEyebtn: UIButton!
    @IBOutlet weak var confirmPassTextField: UITextField!
    @IBOutlet weak var confirmEyeBtn: UIButton!
    @IBOutlet weak var changePassword: UIButton!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

}
