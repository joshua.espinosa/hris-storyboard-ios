//
//  ProfilePageViewController.swift
//  hris-storyboard-ios
//
//  Created by Joshua on 1/8/24.
//

import UIKit
import Alamofire

class ProfilePageViewController: ParentController {
    let getApi = GetApi()
    
    @IBOutlet weak var backBtn: UIImageView!
    @IBOutlet weak var signoutBtn: UIButton!
    @IBOutlet weak var payrollBtn: UIImageView!
    @IBOutlet weak var employeeName: UILabel!
    @IBOutlet weak var employeeAge: UILabel!
    @IBOutlet weak var employeeContact: UILabel!
    @IBOutlet weak var employeeEmail: UILabel!
    @IBOutlet weak var employeeIP: UILabel!
    
    enum LoginError: Error {
        case invalidURL
        case apiError(LoginErrorModel)
        case networkError(Error)
        case decodingError(Error)
        case unknownError
    }

 
    
    deinit {
        Logger.log(message: "Successfully removed ProfilePageViewController from memory")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        //backbtn tapped
        let TapGesture = UITapGestureRecognizer()
        self.backBtn.addGestureRecognizer(TapGesture)
        TapGesture.addTarget(self, action: #selector(tapclick))
//        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
//           backBtn.isUserInteractionEnabled = true
//           backBtn.addGestureRecognizer(tapGestureRecognizer)
        
        //payroll tapped
        let tapPay = UITapGestureRecognizer(target: self, action: #selector(tapFunction(tapGestureRecognizer:)))
           payrollBtn.isUserInteractionEnabled = true
           payrollBtn.addGestureRecognizer(tapPay)
        
        self.processProfile()
        
    }
    

    
    private func processProfile() {
        self.showLoader()
        
        let userId = Session.shared.getStaffNumber()
        
        print("Profile Staff Number \(userId)")
        
        getApi.profileAPI(userId: String(userId)) { result in
            self.hideLoader()
            switch result {
            case .success(let details):
                print("Success: \(String(describing: details))")
             
                self.employeeName.text = details?.data?.name
                self.employeeAge.text = details?.data?.dob
                self.employeeContact.text = details?.data?.mobileNumber
                self.employeeEmail.text = details?.data?.emailAddress
                self.employeeIP.text = details?.data?.regIP
                
              
                
                
            case .failure(let error):
                print("Error: \(error)")
            }
        }
        
    }
    
    //payroll btn
    @objc func tapFunction(tapGestureRecognizer: UITapGestureRecognizer) {
        self.navigationController?.pushViewController(PayrollViewController(), animated: true)
    }
    
    //back btn action
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        self.navigationController?.pushViewController(HomeViewController(), animated: true)
    }
    
    @objc func tapclick(){
        let profileVc = HomeViewController()
        profileVc.modalPresentationStyle = .overCurrentContext
        profileVc.isModalInPresentation = true
        self.present(profileVc, animated: false)
    }

    //sign out btn
    @IBAction func signOut(_ sender: Any) {
        let signOutVc = LoginViewController()
        signOutVc.modalPresentationStyle = .overCurrentContext
        signOutVc.isModalInPresentation = true
        self.present(signOutVc, animated: false)
        //self.navigationController?.pushViewController(LoginViewController(), animated: true)
    }
    
    //time in out history btn
    @IBAction func timeHistory(_ sender: Any) {
        let timeHistoryVc = TimeInOutViewController()
        timeHistoryVc.modalPresentationStyle = .overCurrentContext
        timeHistoryVc.isModalInPresentation = true
        self.present(timeHistoryVc, animated: false)
        //self.navigationController?.pushViewController(TimeInOutViewController(), animated: true)
    }
    
    //overtime btn
    @IBAction func overtimeHistory(_ sender: Any) {
        let overtimeHistoryVc = OverTimeViewController()
        overtimeHistoryVc.modalPresentationStyle = .overCurrentContext
        overtimeHistoryVc.isModalInPresentation = true
        self.present(overtimeHistoryVc, animated: false)
        //self.navigationController?.pushViewController(OverTimeViewController(), animated: true)
    }
    
    //offset btn
    @IBAction func offsetHistory(_ sender: Any) {
        let offsetHistoryVc = OffsetViewController()
        offsetHistoryVc.modalPresentationStyle = .overCurrentContext
        offsetHistoryVc.isModalInPresentation = true
        self.present(offsetHistoryVc, animated: false)
      //  self.navigationController?.pushViewController(OffsetViewController(), animated: true)
    }
    
    //leave btn
    @IBAction func leaveHistory(_ sender: Any) {
        let leaveHistoryVc = LeaveListViewController()
        leaveHistoryVc.modalPresentationStyle = .overCurrentContext
        leaveHistoryVc.isModalInPresentation = true
        self.present(leaveHistoryVc, animated: false)

      // self.navigationController?.pushViewController(LeaveListViewController(), animated: true)
    }

}
