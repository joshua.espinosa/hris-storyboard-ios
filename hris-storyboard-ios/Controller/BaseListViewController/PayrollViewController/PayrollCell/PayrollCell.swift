//
//  PayrollCell.swift
//  hris-storyboard-ios
//
//  Created by Joshua on 1/11/24.
//

import UIKit

class PayrollCell: UITableViewCell {

    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var viewBtn: UIButton!
    
    deinit {
        Logger.log(message: "Successfully removed PayrollCell from memory")
    }
    
    var selectedDate: String? {
        didSet {
            setDate()
        }
    }
    
    
    private func setDate() {
        if let date = selectedDate {
            dateLabel.text = date
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
     
    }
    
    class func createCell(forViewing: Bool? = false) -> PayrollCell? {
        let nib = UINib(nibName: "PayrollCell", bundle: nil)
        let cell = nib.instantiate(withOwner: self, options: nil).last as? PayrollCell
        return cell
    }
    
    
}

