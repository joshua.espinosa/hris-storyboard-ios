//
//  PayrollViewController.swift
//  hris-storyboard-ios
//
//  Created by Joshua on 1/9/24.
//

import UIKit

class PayrollViewController: ParentController {
    
    @IBOutlet weak var backBtn: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    
    var payrollList: [PayrollModel] = []
    var payrollCell: [PayrollCell] = []
    var addedPayroll: [PayrollModel] = []
    
    deinit {
        Logger.log(message: "Successfully removed PayrollViewController from memory")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
           backBtn.isUserInteractionEnabled = true
           backBtn.addGestureRecognizer(tapGestureRecognizer)
        
        //static payroll List
        let testPR1 = PayrollModel(date: "01/20/24")
        let testPR2 = PayrollModel(date: "02/05/24")
        let testPR3 = PayrollModel(date: "02/20/24")
        
        payrollList.append(testPR1)
        payrollList.append(testPR1)
        payrollList.append(testPR1)
      
                          
        setupPayrollCells()
        
    }
    
    private func setupPayrollCells() {
        
        for notif in self.payrollList {
            
            Logger.log(message: "Added Payroll \(notif.date)")
            
            self.payrollCell.append(self.createCell(notif: notif))
            self.tableView.reloadData()
            
        }
    }
    
    //when payroll get clicked
    private func payrollMoreInfo(notif: PayrollModel, index: Int) {
        //must enter pin
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5) {
            self.showPin(action: {
                self.goToPayrollInfo()
            })
        }
        
    }
    
    private func goToPayrollInfo() {
        
        self.navigationController?.pushViewController(PayrollStatusViewController(), animated: true)
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
     
        self.navigationController?.pushViewController(ProfilePageViewController(), animated: true)
      
    }

}

extension PayrollViewController:  UITableViewDelegate, UITableViewDataSource {
    
    func createCell(notif: PayrollModel) -> PayrollCell {
        var cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "PayrollCell") as? PayrollCell
        if cell == nil {
            cell = PayrollCell.createCell()
            cell?.dateLabel.text = notif.date
            
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return (UIDevice.current.userInterfaceIdiom == .pad) ? 70 : 60
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.payrollList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "PayrollCell") as? PayrollCell
        cell = PayrollCell.createCell()
        
        if payrollCell.count > indexPath.row {
            return payrollCell[indexPath.row]
        }
    
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        Logger.log(message: "tap")
        if indexPath.row != self.payrollList.count {
            self.payrollMoreInfo(notif: self.payrollList[indexPath.row], index: indexPath.row)
        }
    }
    
}
