//
//  LeaveListViewController.swift
//  hris-storyboard-ios
//
//  Created by Joshua on 1/9/24.
//

import UIKit

class LeaveListViewController: ParentController {
    
    @IBOutlet weak var backBtn: UIImageView!
    @IBOutlet weak var applyBtn: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    var timeList: [LeaveListModel] = []
    var timeCell: [OvertimeCell] = []
    var addedTime: [LeaveListModel] = []
    
    deinit {
        Logger.log(message: "Successfully removed LeaveListViewController from memory")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
   
//        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
//           backBtn.isUserInteractionEnabled = true
//           backBtn.addGestureRecognizer(tapGestureRecognizer)
        
        let TapGesture = UITapGestureRecognizer()
        self.backBtn.addGestureRecognizer(TapGesture)
        TapGesture.addTarget(self, action: #selector(tapclick))
        tableView.delegate = self
        tableView.dataSource = self
        
        
        applyBtn.makeRoundBorder()
        
        tableView.delegate = self
        tableView.dataSource = self
    
        //static data
        let testData1 = LeaveListModel(dateFrom: "01/25/24", dateUntil: "01/25/24", status: "Approved")
        let testData2 = LeaveListModel(dateFrom: "02/20/24", dateUntil: "02/20/24", status: "Reject")
        let testData3 = LeaveListModel(dateFrom: "03/15/24", dateUntil: "03/15/24", status: "Pending")
        
        
        timeList.append(testData1)
        timeList.append(testData2)
        timeList.append(testData3)
        
        setupHistoryCells()
        
    }
    
    private func setupHistoryCells() {
        
        for data in self.timeList {
            Logger.log(message: "Added History \(data.dateFrom) + \(data.dateUntil)")
            
            self.timeCell.append(self.createCell(data: data))
            self.tableView.reloadData()
            
        }
    }
    
    private func checkIfPending(notif: LeaveListModel, index: Int) {
        
        if notif.status == "Pending"{
            //show pending popup
            self.showPending(action: {
                self.dismiss(animated: false)
            })
        }
        
    }
    
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
       
        self.navigationController?.pushViewController(ProfilePageViewController(), animated: true)
      
    }
    
    @objc func tapclick(){
        let leavelistVc = ProfilePageViewController()
        leavelistVc.modalPresentationStyle = .overCurrentContext
        leavelistVc.isModalInPresentation = true
        self.present(leavelistVc, animated: false)
    }

    @IBAction func applybtn(_ sender: Any) {
        
        let applyVc = ApplyLeaveViewController()
        applyVc.modalPresentationStyle = .overCurrentContext
        applyVc.isModalInPresentation = true
        self.present(applyVc, animated: false)

      //  self.navigationController?.pushViewController(ApplyLeaveViewController(), animated: true)
    }
    
}

extension LeaveListViewController:  UITableViewDelegate, UITableViewDataSource {
    
    func createCell(data: LeaveListModel) -> OvertimeCell {
        var cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "OvertimeCell") as? OvertimeCell
        if cell == nil {
            cell = OvertimeCell.createCell()
            cell?.dateLabel.text = data.dateFrom
            cell?.time.text = data.dateUntil
            cell?.status.text = data.status
            
            if data.status == "Approved" {
                cell?.status.textColor = UIColor.systemGreen
            } else if data.status == "Reject" {
                cell?.status.textColor = UIColor.systemGray
            } else if data.status == "Pending" {
                cell?.status.textColor = UIColor.systemRed
            }
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return (UIDevice.current.userInterfaceIdiom == .pad) ? 70 : 60
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.timeList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "TimeHistoryCell") as? NotificationViewCell
        cell = NotificationViewCell.createCell()
        
        if timeCell.count > indexPath.row {
            return timeCell[indexPath.row]
        }
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        Logger.log(message: "tap")
        if indexPath.row != self.timeList.count {
            self.checkIfPending(notif: self.timeList[indexPath.row], index: indexPath.row)
        }
    }
    
}
