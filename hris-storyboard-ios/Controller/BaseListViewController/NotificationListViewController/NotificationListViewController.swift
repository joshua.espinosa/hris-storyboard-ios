//
//  NotificationListViewController.swift
//  hris-storyboard-ios
//
//  Created by Joshua on 1/10/24.
//

import UIKit

class NotificationListViewController: ParentController {
    
    @IBOutlet weak var backBtn: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    
    var notifsList: [NotificationModel] = []
    var notifCell: [NotificationViewCell] = []
    var addedNotif: [NotificationModel] = []
    
    deinit {
        Logger.log(message: "Successfully removed NotificationListsViewController from memory")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
           backBtn.isUserInteractionEnabled = true
           backBtn.addGestureRecognizer(tapGestureRecognizer)
        
        //static notifications
        let testNotif1 = NotificationModel(date: "01/12/2024", type: "Holiday")
        let testNotif2 = NotificationModel(date: "01/13/2024", type: "Leave")
        let testNotif3 = NotificationModel(date: "01/14/2024", type: "Overtime")
        let testNotif4 = NotificationModel(date: "01/15/2024", type: "Offset")
        let testNotif5 = NotificationModel(date: "01/16/2024", type: "Payroll")
        
        notifsList.append(testNotif1)
        notifsList.append(testNotif2)
        notifsList.append(testNotif3)
        notifsList.append(testNotif4)
        notifsList.append(testNotif5)
                          
        setupNotificationCells()
        
    }
    
    private func setupNotificationCells() {
        
        for notif in self.notifsList {
            Logger.log(message: "Added Notification \(notif.date) + \(notif.type)")
            
            self.notifCell.append(self.createCell(notif: notif))
            self.tableView.reloadData()
            
        }
    }
    
    //when notifs get clicked
    private func notificationMoreInfo(notif: NotificationModel, index: Int) {
        
        if notif.type == "Holiday" {
            Logger.log(message: "Holiday Notif Clicked")
            self.navigationController?.pushViewController(AnnouncementViewController(), animated: true)
        } else if notif.type == "Leave" {
            Logger.log(message: "Leave Notif Clicked")
            self.navigationController?.pushViewController(LeaveStatusViewController(), animated: true)
        } else if notif.type == "Overtime" {
            Logger.log(message: "Overtime Notif Clicked")
            self.navigationController?.pushViewController(OTStatusViewController(), animated: true)
        } else if notif.type == "Offset" {
            Logger.log(message: "Offset Notif Clicked")
            self.navigationController?.pushViewController(OffsetStatusViewController(), animated: true)
        } else if notif.type == "Payroll" {
            Logger.log(message: "Payroll Notif Clicked")
            
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5) {
                self.showPin(action: {
                    self.goToPayrollInfo()
                })
            }
        }
        
    }
    
    private func goToPayrollInfo() {
        self.navigationController?.pushViewController(PayrollStatusViewController(), animated: true)
    }
    
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {

        self.navigationController?.pushViewController(HomeViewController(), animated: true)
    }
    
    
}

extension NotificationListViewController:  UITableViewDelegate, UITableViewDataSource {
    
    func createCell(notif: NotificationModel) -> NotificationViewCell {
        var cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "NotificationViewCell") as? NotificationViewCell
        if cell == nil {
            cell = NotificationViewCell.createCell()
            cell?.dateLabel.text = notif.date
            cell?.typeLabel.text = notif.type
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return (UIDevice.current.userInterfaceIdiom == .pad) ? 70 : 60
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.notifsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "NotificationViewCell") as? NotificationViewCell
        cell = NotificationViewCell.createCell()
        
        if notifCell.count > indexPath.row {
            return notifCell[indexPath.row]
        }
    
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        Logger.log(message: "tap")
        if indexPath.row != self.notifsList.count {
            self.notificationMoreInfo(notif: self.notifsList[indexPath.row], index: indexPath.row)
        }
    }
    
}
