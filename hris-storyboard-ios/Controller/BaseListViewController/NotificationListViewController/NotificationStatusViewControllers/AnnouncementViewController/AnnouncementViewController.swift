//
//  AnnouncementViewController.swift
//  hris-storyboard-ios
//
//  Created by Joshua on 1/11/24.
//

import UIKit

class AnnouncementViewController: ParentController {
    
    @IBOutlet weak var backBtn: UIImageView!
    @IBOutlet weak var contentView: UIView!
    
    deinit {
        Logger.log(message: "Successfully removed AnnouncementViewController from memory")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
           backBtn.isUserInteractionEnabled = true
           backBtn.addGestureRecognizer(tapGestureRecognizer)
        
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {

        self.navigationController?.pushViewController(NotificationListViewController(), animated: true)
      
    }
    
    
    
}
