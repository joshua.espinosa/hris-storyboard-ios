//
//  PayrollStatusViewController.swift
//  hris-storyboard-ios
//
//  Created by Joshua on 1/11/24.
//

import UIKit

class PayrollStatusViewController: ParentController {
    
    @IBOutlet weak var backBtn: UIImageView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var acknowledgeBtn: UIButton!
    
    deinit {
        Logger.log(message: "Successfully removed PayrollStatusViewController from memory")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        acknowledgeBtn.makeRoundBorder()
        acknowledgeBtn.tintColor = UIColor.white
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
           backBtn.isUserInteractionEnabled = true
           backBtn.addGestureRecognizer(tapGestureRecognizer)
        
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {

        self.navigationController?.pushViewController(NotificationListViewController(), animated: true)
      
    }
    
    
    
}
