//
//  NotificationViewCell.swift
//  hris-storyboard-ios
//
//  Created by Joshua on 1/10/24.
//

import UIKit

class NotificationViewCell: UITableViewCell {

    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var viewBtn: UIButton!
    
    static let Name = "NotificationViewCell"

    
    deinit {
        Logger.log(message: "Successfully removed NotificationViewCell from memory")
    }
    
    var notificationsData: notifLists? {
        didSet {
            self.setNoficationsInfo()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.layer.cornerRadius = 10
        self.layer.masksToBounds = false

        self.layer.shadowOpacity = 1
        self.layer.shadowRadius = 5
    }
    
    private func setNoficationsInfo() {
        guard let notifInfo = notificationsData else {
            return
        }
        self.dateLabel.text = "\(notifInfo.date ?? "")"
        self.typeLabel.text = "\(notifInfo.notifType ?? "")"
        
    }
    
    var selectedDate: String? {
        didSet {
            setDate()
        }
    }
    
    var selectedType: String? {
        didSet {
            setType()
        }
    }
    
    private func setDate() {
        if let date = selectedDate {
            dateLabel.text = date
        }
    }
    
    private func setType() {
        if let type = selectedType {
            typeLabel.text = type
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    
    class func createCell(forViewing: Bool? = false) -> NotificationViewCell? {
        let nib = UINib(nibName: "NotificationViewCell", bundle: nil)
        let cell = nib.instantiate(withOwner: self, options: nil).last as? NotificationViewCell
        return cell
    }
    
    
}

