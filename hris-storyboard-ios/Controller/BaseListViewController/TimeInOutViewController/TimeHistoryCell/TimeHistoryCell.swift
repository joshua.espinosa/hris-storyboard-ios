//
//  TimeHistoryCell.swift
//  hris-storyboard-ios
//
//  Created by Joshua on 1/11/24.
//

import UIKit

class TimeHistoryCell: UITableViewCell {

    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeIn: UILabel!
    @IBOutlet weak var timeOut: UILabel!
    @IBOutlet weak var status: UILabel!
    
    deinit {
        Logger.log(message: "Successfully removed TimeHistoryCell from memory")
    }
    
    var selectedDate: String? {
        didSet {
            setDate()
        }
    }
    
    var selectedTimeIn: String? {
        didSet {
            setTimeIn()
        }
    }
    
    var selectedTimeOut: String? {
        didSet {
            setTimeIn()
        }
    }
    
    var selectedStatus: String? {
        didSet {
            setStatus()
        }
    }
    
    private func setDate() {
        if let date = selectedDate {
            dateLabel.text = date
        }
    }
    
    private func setTimeIn() {
        if let timeInString = selectedTimeIn {
            timeIn.text = timeInString
        }
    }
    
    private func setTimeOut() {
        if let timeOutString = selectedTimeOut {
            timeOut.text = timeOutString
        }
    }
    
    private func setStatus() {
        if let statusString = selectedStatus {
            
           
            status.text = statusString
        }
    }
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
     
    }
    
    class func createCell(forViewing: Bool? = false) -> TimeHistoryCell? {
        let nib = UINib(nibName: "TimeHistoryCell", bundle: nil)
        let cell = nib.instantiate(withOwner: self, options: nil).last as? TimeHistoryCell
        return cell
    }
    
    
}

