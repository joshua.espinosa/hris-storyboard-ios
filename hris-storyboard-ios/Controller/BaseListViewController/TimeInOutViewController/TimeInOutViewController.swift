//
//  TimeInOutViewController.swift
//  hris-storyboard-ios
//
//  Created by Joshua on 1/9/24.
//

import UIKit


class TimeInOutViewController: ParentController {
    
    @IBOutlet weak var backBtn: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    
    var timeList: [TimeHistoryModel] = []
    var timeCell: [TimeHistoryCell] = []
    var addedTime: [TimeHistoryModel] = []
    
    deinit {
        Logger.log(message: "Successfully removed TimeInOutViewController from memory")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
//           backBtn.isUserInteractionEnabled = true
//           backBtn.addGestureRecognizer(tapGestureRecognizer)
        
        let TapGesture = UITapGestureRecognizer()
        self.backBtn.addGestureRecognizer(TapGesture)
        TapGesture.addTarget(self, action: #selector(tapclick))
        tableView.delegate = self
        tableView.dataSource = self
        
        //static data
        let testData1 = TimeHistoryModel(date: "01/21/24", timeIn: "8:00", timeOut: "5:00", status: "Complete")
        let testData2 = TimeHistoryModel(date: "01/23/24", timeIn: "8:00", timeOut: "missing", status: "No Time Out")
        let testData3 = TimeHistoryModel(date: "01/25/24", timeIn: "missing", timeOut: "missing", status: "Absent")

        timeList.append(testData1)
        timeList.append(testData2)
        timeList.append(testData3)
        
        setupHistoryCells()
        
        
    }
    
    private func setupHistoryCells() {
        
        for data in self.timeList {
            Logger.log(message: "Added History \(data.date) + \(data.status)")
            
            self.timeCell.append(self.createCell(data: data))
            self.tableView.reloadData()
            
        }
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        self.navigationController?.pushViewController(ProfilePageViewController(), animated: true)
    }
    
    @objc func tapclick(){
        let timeinoutVc = ProfilePageViewController()
        timeinoutVc.modalPresentationStyle = .overCurrentContext
        timeinoutVc.isModalInPresentation = true
        self.present(timeinoutVc, animated: false)
    }
    
}

extension TimeInOutViewController:  UITableViewDelegate, UITableViewDataSource {
    
    func createCell(data: TimeHistoryModel) -> TimeHistoryCell {
        var cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "TimeHistoryCell") as? TimeHistoryCell
        if cell == nil {
            cell = TimeHistoryCell.createCell()
            cell?.dateLabel.text = data.date
            cell?.timeIn.text = data.timeIn
            cell?.timeOut.text = data.timeOut
            cell?.status.text = data.status
            
            if data.status == "Complete" {
                cell?.status.textColor = UIColor.systemGreen
            } else if data.status == "No Time Out" {
                cell?.status.textColor = UIColor.systemYellow
            } else if data.status == "Absent" {
                cell?.status.textColor = UIColor.systemRed
            }
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return (UIDevice.current.userInterfaceIdiom == .pad) ? 70 : 60
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.timeList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "TimeHistoryCell") as? NotificationViewCell
        cell = NotificationViewCell.createCell()
        
        if timeCell.count > indexPath.row {
            return timeCell[indexPath.row]
        }
    
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        Logger.log(message: "tap")
        if indexPath.row != self.timeList.count {
           
        }
    }
    
}
