//
//  ApplyLeaveViewController.swift
//  hris-storyboard-ios
//
//  Created by Joshua on 1/12/24.
//

import UIKit

class ApplyLeaveViewController: ParentController {
    
    @IBOutlet weak var backBtn: UIImageView!
    @IBOutlet weak var nextBtn: UIButton!
    
    deinit {
        Logger.log(message: "Successfully removed ApplyLeaveViewController from memory")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        //backbtn tapped
//        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
//           backBtn.isUserInteractionEnabled = true
//           backBtn.addGestureRecognizer(tapGestureRecognizer)
        
        let TapGesture = UITapGestureRecognizer()
        self.backBtn.addGestureRecognizer(TapGesture)
        TapGesture.addTarget(self, action: #selector(tapclick))
        
        nextBtn.makeRoundBorder()
        
    }
    
    //back btn action
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        self.navigationController?.pushViewController(LeaveListViewController(), animated: true)
    }
    
    @objc func tapclick(){
        let applyleaveVc = LeaveListViewController()
        applyleaveVc.modalPresentationStyle = .overCurrentContext
        applyleaveVc.isModalInPresentation = true
        self.present(applyleaveVc, animated: false)
    }

    @IBAction func nextBtn(_ sender: Any) {
        let nextBtnVc = NextLeaveViewController()
        nextBtnVc.modalPresentationStyle = .overCurrentContext
        nextBtnVc.isModalInPresentation = true
        self.present(nextBtnVc, animated: false)
//        self.navigationController?.pushViewController(NextLeaveViewController(), animated: true)
    }
}
