//
//  NextLeaveViewController.swift
//  hris-storyboard-ios
//
//  Created by Joshua on 1/12/24.
//

import UIKit

class NextLeaveViewController: ParentController {
    
    @IBOutlet weak var backBtn: UIImageView!
    @IBOutlet weak var submitBtn: UIButton!
    
    deinit {
        Logger.log(message: "Successfully removed NextLeaveViewController from memory")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        //backbtn tapped
        //        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        //           backBtn.isUserInteractionEnabled = true
        //           backBtn.addGestureRecognizer(tapGestureRecognizer)
        let TapGesture = UITapGestureRecognizer()
        self.backBtn.addGestureRecognizer(TapGesture)
        TapGesture.addTarget(self, action: #selector(tapclick))
        
        submitBtn.makeRoundBorder()
        
    }
    
    //back btn action
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        self.navigationController?.pushViewController(ApplyLeaveViewController(), animated: true)
    }
    
    @objc func tapclick(){
        let nextVc = ApplyLeaveViewController()
        nextVc.modalPresentationStyle = .overCurrentContext
        nextVc.isModalInPresentation = true
        self.present(nextVc, animated: false)
        
    }
}
