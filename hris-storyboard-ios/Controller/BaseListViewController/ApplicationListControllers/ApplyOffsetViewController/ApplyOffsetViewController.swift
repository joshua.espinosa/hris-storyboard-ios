//
//  ApplyOffsetViewController.swift
//  hris-storyboard-ios
//
//  Created by Joshua on 1/12/24.
//

import UIKit

class ApplyOffsetViewController: ParentController {
    
    @IBOutlet weak var backBtn: UIImageView!
    @IBOutlet weak var reasonView: UIView!
    @IBOutlet weak var applyBtn: UIButton!
    
    deinit {
        Logger.log(message: "Successfully removed ApplyOffsetViewController from memory")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        applyBtn.makeRoundBorder()
        
        //backbtn tapped
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
           backBtn.isUserInteractionEnabled = true
           backBtn.addGestureRecognizer(tapGestureRecognizer)
        
    }
    
    //back btn action
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        self.navigationController?.pushViewController(OffsetViewController(), animated: true)
    }

    
    @IBAction func applybtn(_ sender: Any) {
        
    }
    
    
}
