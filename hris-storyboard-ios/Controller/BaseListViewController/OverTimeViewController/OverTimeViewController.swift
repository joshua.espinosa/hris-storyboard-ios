//
//  OverTimeViewController.swift
//  hris-storyboard-ios
//
//  Created by Joshua on 1/9/24.
//

import UIKit

class OverTimeViewController: ParentController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var backBtn: UIImageView!
    @IBOutlet weak var applyBtn: UIButton!
    
    var timeList: [OffsetModel] = []
    var timeCell: [OvertimeCell] = []
    var addedTime: [OffsetModel] = []
    
    deinit {
        Logger.log(message: "Successfully removed OverTimeViewController from memory")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
//           backBtn.isUserInteractionEnabled = true
//           backBtn.addGestureRecognizer(tapGestureRecognizer)
        
        let TapGesture = UITapGestureRecognizer()
        self.backBtn.addGestureRecognizer(TapGesture)
        TapGesture.addTarget(self, action: #selector(tapclick))
        tableView.delegate = self
        tableView.dataSource = self
        
        
        applyBtn.makeRoundBorder()
        tableView.delegate = self
        tableView.dataSource = self
        
        //static data
        let testData1 = OffsetModel(date: "01/21/24", time: "6:00 PM ~ 7:00 PM", status: "Approved")
        let testData2 = OffsetModel(date: "01/23/24", time: "6:00 PM ~ 7:00 PM", status: "Reject")
        let testData3 = OffsetModel(date: "01/25/24", time: "6:00 PM ~ 7:00 PM", status: "Pending")

        timeList.append(testData1)
        timeList.append(testData2)
        timeList.append(testData3)
        
        setupHistoryCells()
        
    }
    
    private func setupHistoryCells() {
        
        for data in self.timeList {
            Logger.log(message: "Added History \(data.date) + \(data.status)")
            
            self.timeCell.append(self.createCell(data: data))
            self.tableView.reloadData()
            
        }
    }
    
    @IBAction func applyBtn(_ sender: Any) {
        let applyBtnVc = ApplyOTViewController()
        applyBtnVc.modalPresentationStyle = .overCurrentContext
        applyBtnVc.isModalInPresentation = true
        self.present(applyBtnVc, animated: false)
       // self.navigationController?.pushViewController(ApplyOTViewController(), animated: true)
        
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {

        self.navigationController?.pushViewController(ProfilePageViewController(), animated: true)
      
    }
    
    @objc func tapclick(){
        let overtimeVc = ProfilePageViewController()
        overtimeVc.modalPresentationStyle = .overCurrentContext
        overtimeVc.isModalInPresentation = true
        self.present(overtimeVc, animated: false)
    }
    
    
    private func checkIfPending(notif: OffsetModel, index: Int) {
        
        if notif.status == "Pending"{
            //show pending popup
            
            self.showPending(action: {
                self.dismiss(animated: false)
            })
        }
        
    }
    
    
}

extension OverTimeViewController:  UITableViewDelegate, UITableViewDataSource {
    
    func createCell(data: OffsetModel) -> OvertimeCell {
        var cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "OvertimeCell") as? OvertimeCell
        if cell == nil {
            cell = OvertimeCell.createCell()
            cell?.dateLabel.text = data.date
            cell?.time.text = data.time
            cell?.status.text = data.status
            
            if data.status == "Approved" {
                cell?.status.textColor = UIColor.systemGreen
            } else if data.status == "Reject" {
                cell?.status.textColor = UIColor.systemGray
            } else if data.status == "Pending" {
                cell?.status.textColor = UIColor.systemRed
            }
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return (UIDevice.current.userInterfaceIdiom == .pad) ? 70 : 60
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.timeList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "TimeHistoryCell") as? NotificationViewCell
        cell = NotificationViewCell.createCell()
        
        if timeCell.count > indexPath.row {
            return timeCell[indexPath.row]
        }
    
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        Logger.log(message: "tap")
        if indexPath.row != self.timeList.count {
            self.checkIfPending(notif: self.timeList[indexPath.row], index: indexPath.row)
        }
    }
    
}
