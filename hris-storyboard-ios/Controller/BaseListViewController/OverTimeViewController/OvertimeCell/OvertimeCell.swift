//
//  OvertimeCell.swift
//  hris-storyboard-ios
//
//  Created by Joshua on 1/11/24.
//


import UIKit

class OvertimeCell: UITableViewCell {

    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var status: UILabel!
    

    
    deinit {
        Logger.log(message: "Successfully removed OvertimeCell from memory")
    }
    
    var selectedDate: String? {
        didSet {
            setDate()
        }
    }
    
    var selectedTime: String? {
        didSet {
            setTime()
        }
    }
    
    var selectedStatus: String? {
        didSet {
            setStatus()
        }
    }
    
    private func setDate() {
        if let date = selectedDate {
            dateLabel.text = date
        }
    }
    
    private func setTime() {
        if let timeString = selectedTime {
            time.text = timeString
        }
    }
    
    
    private func setStatus() {
        if let statusString = selectedStatus {
            
            if statusString == "Approved" {
                status.textColor = UIColor.systemGreen
            } else if statusString == "Reject" {
                status.textColor = UIColor.systemGray
            } else if statusString == "Pending" {
                status.textColor = UIColor.systemRed
            }
            
            status.text = statusString
        }
    }
    

    override func awakeFromNib() {
        super.awakeFromNib()
     
    }
    
    class func createCell(forViewing: Bool? = false) -> OvertimeCell? {
        let nib = UINib(nibName: "OvertimeCell", bundle: nil)
        let cell = nib.instantiate(withOwner: self, options: nil).last as? OvertimeCell
        return cell
    }
    
    
}

