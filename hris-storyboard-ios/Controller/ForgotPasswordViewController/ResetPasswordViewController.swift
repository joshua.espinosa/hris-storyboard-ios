//
//  ResetPasswordViewController.swift
//  hris-storyboard-ios
//
//  Created by Gemmalyn Medinaceli on 1/15/24.
//

import UIKit

class ResetPasswordViewController: ParentController, UITextFieldDelegate {
    let postApi = PostApi()
    
    @IBOutlet weak var passwordBtn: UIButton!
    
    @IBOutlet weak var confirmPasswordBtn: UIButton!
    @IBOutlet weak var tokenTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    
    @IBOutlet weak var resetButton: UIButton!
    @IBOutlet weak var eyePasswordBtn: UIButton!
    @IBOutlet weak var eyeConfirmPasswordBtn: UIButton!
    
    @IBOutlet weak var tokenMessage: UILabel!
    @IBOutlet weak var passwordNotMatch: UILabel!
    
    //Validation
    @IBOutlet weak var minimumCharacters: UILabel!
    @IBOutlet weak var numberCharacter: UILabel!
    @IBOutlet weak var upperCase: UILabel!
    @IBOutlet weak var lowerCase: UILabel!
    @IBOutlet weak var specialCharacter: UILabel!
    
    var iconClickpassword = true
    var iconClickConfirmPassword = true
    
    var stringLength = false
    var numberString = false
    var uppercaseString = false
    var lowercaseString = false
    var specialString = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tokenTextField.delegate = self
        passwordTextField.delegate = self
        confirmPasswordTextField.delegate = self
        
        resetButton.isEnabled = false
        
        
        // Do any additional setup after loading the view.
        
        passwordNotMatch.isHidden = true
        passwordTextField.isSecureTextEntry = true
        confirmPasswordTextField.isSecureTextEntry = true
        
        print(stringLength)
        
    }
    
    @IBAction func resetPasswordBtnAction(_ sender: Any) {
        
        guard let token = tokenTextField.text else {
            return
        }
        guard let password = confirmPasswordTextField.text else {
            return
        }
        resetPass(token: token, password: password)
        
    }
    
    @IBAction func passwordActionBtn(_ sender: Any) {
        viewPassword()
    }
    
    @IBAction func confirmPasswordActionBtn(_ sender: Any) {
      viewConfirmPassword()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
       
        //Token textField
        if(textField == tokenTextField) {
            
          
            //String Limit
            let maxLength = 8
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =  currentString.replacingCharacters(in: range, with: string) as NSString

            if newString.length > 0 {
                tokenMessage.isHidden = true
            } else {
                tokenMessage.isHidden = false
            }
            
             return newString.length <= maxLength
            
        }
        
        //Password TextField
        if(textField == passwordTextField){
          let strLength = textField.text?.count ?? 0
          let lngthToAdd = string.count
          let lengthCount = strLength + lngthToAdd
           print(lengthCount)
            
            if lengthCount >= 8 {
                minimumCharacters.textColor = UIColor(red: 0/255, green: 143/255, blue: 0/255, alpha: 1)
                 stringLength = true
            } else {
                minimumCharacters.textColor = UIColor(red: 170/255, green: 170/255, blue: 170/255, alpha: 1)
                stringLength = false
            }
            
            let passwordText = (textField.text as NSString?)?.replacingCharacters(in: range, with: string) ?? string
                    
                    // Perform actions based on the detected characters
                    if passwordText.rangeOfCharacter(from: .decimalDigits) != nil {
                        numberCharacter.textColor = UIColor(red: 0/255, green: 143/255, blue: 0/255, alpha: 1)
                            numberString = true
                    } else {
                        numberCharacter.textColor = UIColor(red: 170/255, green: 170/255, blue: 170/255, alpha: 1)
                        numberString = false
                        resetButton.isEnabled = false
                    }
                    if passwordText.rangeOfCharacter(from: .uppercaseLetters) != nil {
                        upperCase.textColor = UIColor(red: 0/255, green: 143/255, blue: 0/255, alpha: 1)
                        uppercaseString = true
                    } else {
                        upperCase.textColor = UIColor(red: 170/255, green: 170/255, blue: 170/255, alpha: 1)
                        uppercaseString = false
                        resetButton.isEnabled = false
                    }
                    if passwordText.rangeOfCharacter(from: .lowercaseLetters) != nil {
                        lowerCase.textColor = UIColor(red: 0/255, green: 143/255, blue: 0/255, alpha: 1)
                        lowercaseString = true
                    } else {
                        lowerCase.textColor = UIColor(red: 170/255, green: 170/255, blue: 170/255, alpha: 1)
                        lowercaseString = false
                        resetButton.isEnabled = false
                    }
                    if passwordText.rangeOfCharacter(from: .punctuationCharacters) != nil {
                        specialCharacter.textColor = UIColor(red: 0/255, green: 143/255, blue: 0/255, alpha: 1)
                        specialString = true
                    } else {
                        specialCharacter.textColor = UIColor(red: 170/255, green: 170/255, blue: 170/255, alpha: 1)
                        specialString = false
                        resetButton.isEnabled = false
                    }
            
            guard let confirmPasswordText = confirmPasswordTextField.text else { return true }
            print("password: \(textField)")
                    
            if passwordText == confirmPasswordText {
                passwordNotMatch.isHidden = true
                resetButton.isEnabled = true
            } else {
                passwordNotMatch.isHidden = false
                resetButton.isEnabled = false
            }
       }
        
        //Confirm Password TextField
        if (textField == confirmPasswordTextField) {
            
            guard let tokenText = tokenTextField.text else {return true}
            
            guard let passwordText = passwordTextField.text else { return true }
            print("password: \(textField)")
            
            let confirmText = (textField.text as NSString?)?.replacingCharacters(in: range, with: string) ?? string
            
            if confirmText == passwordText {
                passwordNotMatch.isHidden = true
                
                if stringLength == true && numberString == true && uppercaseString == true && lowercaseString == true && specialString == true {
                    resetButton.isEnabled = true
                } else {
                    resetButton.isEnabled = false
                }
                
            } else {
                passwordNotMatch.isHidden = false
                resetButton.isEnabled = false
            }
        }
       return true
    }
  
    //To view Password
    func viewPassword() {
        if iconClickpassword {
            passwordTextField.isSecureTextEntry = false
            eyePasswordBtn.setImage(UIImage(systemName: "eye.slash"), for: .normal)
        } else {
            passwordTextField.isSecureTextEntry = true
            eyePasswordBtn.setImage(UIImage(systemName: "eye"), for: .normal)
        }
        iconClickpassword = !iconClickpassword
    }
    
    //To View Confirm Password
    func viewConfirmPassword() {
        if iconClickConfirmPassword {
            confirmPasswordTextField.isSecureTextEntry = false
            eyeConfirmPasswordBtn.setImage(UIImage(systemName: "eye.slash"), for: .normal)
        } else {
            confirmPasswordTextField.isSecureTextEntry = true
            eyeConfirmPasswordBtn.setImage(UIImage(systemName: "eye"), for: .normal)
        }
        iconClickConfirmPassword = !iconClickConfirmPassword
    }
    
    func resetPass(token: String, password: String) {
        showLoader()
        postApi.resetPasswordAPI(token: token, password: password) { result in
            self.hideLoader()
            switch result {
            case .success(let forgotResponse):
                print("Success: \(forgotResponse)")
                Session.shared.setResetMessage(fMessage: forgotResponse?.message ?? "")
                
                    let homeVc = UINavigationController(rootViewController: LoginViewController())
                        homeVc.modalPresentationStyle = .fullScreen
                self.present(homeVc, animated: false, completion: nil )
            case .failure(let error):
            print("Error: \(error)")
            }
        }
    }

}
