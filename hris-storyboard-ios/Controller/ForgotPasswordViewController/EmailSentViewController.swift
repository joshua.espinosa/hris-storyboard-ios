//
//  EmailSentViewController.swift
//  hris-storyboard-ios
//
//  Created by Gemmalyn Medinaceli on 1/15/24.
//

import UIKit

class EmailSentViewController: UIViewController {

    @IBOutlet var emailSent: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func okBtnAction(_ sender: Any) {
        let noticeVc = ResetPasswordViewController()
        noticeVc.modalPresentationStyle = .overCurrentContext
        noticeVc.isModalInPresentation = true
        self.present(noticeVc, animated: false, completion: {
            self.emailSent.alpha = 0
    })
        }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
