//
//  ForgotPasswordViewController.swift
//  hris-storyboard-ios
//
//  Created by Gemmalyn Medinaceli on 1/4/24.
//

import UIKit
import Foundation
import Alamofire

class ForgotPasswordViewController: ParentController, UITextFieldDelegate {
   
   let postApi = PostApi()

    @IBOutlet weak var dismissView: UIView!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet var forgotPassword: UIView!
    
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var notValid: UILabel!
    
    deinit {
        Logger.log(message: "Successfully removed ForgotPasswordViewController from memory")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        emailTextField.delegate = self
        notValid.isHidden = true
        forgotPassword.alpha = 1
        
        let TapGesture = UITapGestureRecognizer()
        self.dismissView.addGestureRecognizer(TapGesture)
        TapGesture.addTarget(self, action: #selector(tapclick))
       
        
    }
    
    
  
    @IBAction func submitBtnAction(_ sender: Any) {
       
        
        guard let email = emailTextField.text, !email.isEmpty else {
                return
            }
        forgotPass(email: email)
    }
    

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
          
        if textField == emailTextField {
              // Validate email format as user types
              let currentText = (textField.text ?? "") as NSString
              let newText = currentText.replacingCharacters(in: range, with: string)
              if isValidEmail(email: newText) {
                  // Valid email format
                  notValid.isHidden = true
                  submitBtn.isEnabled = true
                  
              } else {
                  // Invalid email format
                  notValid.isHidden = false
                  submitBtn.isEnabled = false
              }
          }
          return true
      }
    
    func forgotPass(email: String) {
        showLoader()
        postApi.forgotPasswordAPI(email: email){ result in
            self.hideLoader()
            switch result {
            case .success(let forgotResponse):
                print("Success: \(forgotResponse?.message)")
                if ((forgotResponse?.message?.contains("Password")) == true) {
                    let noticeVc = EmailSentViewController()
                            noticeVc.modalPresentationStyle = .overCurrentContext
                            noticeVc.isModalInPresentation = true
                            self.present(noticeVc, animated: false, completion: {
                            self.forgotPassword.alpha = 0

                            })
                } else {
                    print("Success: \(forgotResponse?.message)")
                }
            case .failure(let error):
                print("Error: \(error)")
                }
            }
        }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
           // Dismiss keyboard when return key is pressed
           textField.resignFirstResponder()
           return true
       }
       
       // MARK: - Helper
       
       func isValidEmail(email: String) -> Bool {
           // Regular expression to validate email format
           let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
           let emailPredicate = NSPredicate(format: "SELF MATCHES %@", emailRegex)
           return emailPredicate.evaluate(with: email)
       }
    
    //tapclick function
    @objc func tapclick() {
        dismiss(animated: false, completion: nil)
    }

}
