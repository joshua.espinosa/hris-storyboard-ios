//
//  LoaderViewController.swift
//  hris-storyboard-ios
//
//  Created by Joshua on 1/3/24.
//

import UIKit
import Alamofire

class LoaderViewController: ParentController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
//    private func goToHomePage() {
//        let homeVc = UINavigationController(rootViewController: HomeViewController())
//        homeVc.modalPresentationStyle = .fullScreen
//        self.present(homeVc, animated: true, completion: nil)
//    }
//    
//    private func goToAuthPage() {
//        let authVc = UINavigationController(rootViewController: AuthViewController())
//        authVc.modalPresentationStyle = .fullScreen
//        self.present(authVc, animated: false, completion: nil)
//    }
}
