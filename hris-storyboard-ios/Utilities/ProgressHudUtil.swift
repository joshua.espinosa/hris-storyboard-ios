//
//  ProgressHudUtil.swift
//  hris-storyboard-ios
//
//  Created by Joshua on 1/3/24.
//

import Foundation
import SVProgressHUD

class ProgressHudUtil {
    public static func show(message: String) {
        SVProgressHUD.setDefaultMaskType(.black)
        SVProgressHUD.showInfo(withStatus: message)
        SVProgressHUD.dismiss(withDelay: 1.0)
    }
    
    public static func show(message: String, duration: Double) {
        SVProgressHUD.setDefaultMaskType(.black)
        SVProgressHUD.showInfo(withStatus: message)
        SVProgressHUD.dismiss(withDelay: duration)
    }
    
    public static func showError(message: String) {
        SVProgressHUD.setDefaultMaskType(.black)
        SVProgressHUD.showError(withStatus: message)
        SVProgressHUD.dismiss(withDelay: 1.0)
    }
    
    public static func showError(message: String, duration: Double) {
        SVProgressHUD.setDefaultMaskType(.black)
        SVProgressHUD.showError(withStatus: message)
        SVProgressHUD.dismiss(withDelay: duration)
    }
    
    public static func showLoader(message: String) {
        SVProgressHUD.setDefaultMaskType(.black)
        SVProgressHUD.show(withStatus: message)
    }
    
    public static func hideLoader() {
        SVProgressHUD.dismiss()
    }
}
