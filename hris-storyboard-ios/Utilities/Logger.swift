//
//  Logger.swift
//  hris-storyboard-ios
//
//  Created by Joshua on 1/3/24.
//

import Foundation
import UIKit

public class Logger {

    private static var isDebug = true;

    public static func initializeLogger(forceDebug: Bool) {
        if forceDebug {
            isDebug = true
        }
        log(message: "Initialize \(isDebug ? "Debug" : "Prod") Mode.")
    }
    
    public static func disableDebug() {
        isDebug = false;
    }
    
    public static func isDebugMode() -> Bool {
        return isDebug
    }
    
    public static func log(message: String) {
        if isDebug {
            print(message)
        }
    }
    
    public static func showToast(controller: UIViewController, message : String, seconds: Double) {
        if isDebug {
            let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
            alert.view.backgroundColor = UIColor.black
            alert.view.alpha = 0.6
            alert.view.layer.cornerRadius = 15

            controller.present(alert, animated: true)

            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + seconds) {
                alert.dismiss(animated: true)
            }
        }
    }
}
