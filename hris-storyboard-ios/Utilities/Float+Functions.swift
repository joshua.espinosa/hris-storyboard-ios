//
//  Float+Functions.swift
//  hris-storyboard-ios
//
//  Created by Joshua on 1/3/24.
//

import Foundation

extension Float {
    var clean: String {
       return self.truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.0f", self) : String(self)
    }
    
    var showTwoDecimals: String {
        return String(format: "%.2f", self)
    }
}

extension Double {
    var showTwoDecimals: String {
        return String(format: "%.2f", self)
    }
}
