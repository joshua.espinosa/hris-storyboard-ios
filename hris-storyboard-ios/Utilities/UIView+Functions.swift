//
//  UIView+Functions.swift
//  hris-storyboard-ios
//
//  Created by Joshua on 1/3/24.
//

import UIKit

extension UIView {
    func copyObject<T:NSObject>() throws -> T? {
        let data = try NSKeyedArchiver.archivedData(withRootObject:self, requiringSecureCoding:false)
        return try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data) as? T
    }
    
    func addDashedBorder(color: CGColor) -> CAShapeLayer {
        let shapeLayer:CAShapeLayer = CAShapeLayer()
        let frameSize = self.frame.size
        let shapeRect = CGRect(x: 0, y: 0, width: frameSize.width, height: frameSize.height)
        
        shapeLayer.bounds = shapeRect
        shapeLayer.position = CGPoint(x: frameSize.width/2, y: frameSize.height/2)
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = color
        shapeLayer.lineWidth = 1
        shapeLayer.lineJoin = CAShapeLayerLineJoin.round
        shapeLayer.lineDashPattern = [12,5]
        shapeLayer.path = UIBezierPath(roundedRect: shapeRect, cornerRadius: 10).cgPath
        
        self.layer.addSublayer(shapeLayer)
        return shapeLayer
    }
    
    func addDashedBorder(width: CGFloat? = 4) {
        let color = UIColor.black.cgColor

        let shapeLayer:CAShapeLayer = CAShapeLayer()
        let frameSize = self.frame.size
        let shapeRect = CGRect(x: 0, y: 0, width: frameSize.width, height: frameSize.height)

        shapeLayer.bounds = shapeRect
        shapeLayer.position = CGPoint(x: frameSize.width/2, y: frameSize.height/2)
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = color
        shapeLayer.lineWidth = width ?? 1
        shapeLayer.lineJoin = CAShapeLayerLineJoin.round
        shapeLayer.lineDashPattern = [12, 3]
        shapeLayer.path = UIBezierPath(roundedRect: shapeRect, cornerRadius: 5).cgPath

        self.layer.addSublayer(shapeLayer)
    }
    
    func addDashBorder(width: CGFloat? = 4) {
        let color = UIColor.black.cgColor

        let shapeLayer:CAShapeLayer = CAShapeLayer()
        let frameSize = self.frame.size
        let shapeRect = CGRect(x: 0, y: 0, width: frameSize.width - 10, height: frameSize.height)

        shapeLayer.bounds = shapeRect
        shapeLayer.position = CGPoint(x: frameSize.width/2, y: frameSize.height/2)
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = color
        shapeLayer.lineWidth = width ?? 1
        shapeLayer.lineJoin = CAShapeLayerLineJoin.round
        shapeLayer.lineDashPattern = [12, 3]
        shapeLayer.path = UIBezierPath(roundedRect: shapeRect, cornerRadius: 5).cgPath

        self.layer.addSublayer(shapeLayer)
    }
    func addBottomShadow() {
        self.layer.masksToBounds = false
        self.layer.shadowRadius = 4
        self.layer.shadowOpacity = 1
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOffset = CGSize(width: 0 , height:2)
    }
    
    func makeRoundBorder() {
        self.layer.cornerRadius = CGFloat(Constants.Design.ButtonCorderRadius)
    }
    func makeUpperCornerRound() {
        self.layer.cornerRadius = 10
        self.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        self.layer.masksToBounds = false
        self.layer.shadowOpacity = 0.6

    }
    
    func makeRoundBorder(value: CGFloat) {
        self.layer.cornerRadius = value
    }
    
    func makeTopRound(radius: CGFloat? = nil) {
        self.clipsToBounds = true
        self.layer.cornerRadius = radius ?? CGFloat(Constants.Design.ButtonCorderRadius)
        self.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
    }
    
    func makeBottomRound(radius: CGFloat? = nil) {
        self.clipsToBounds = true
        self.layer.cornerRadius = radius ?? CGFloat(Constants.Design.ButtonCorderRadius)
        self.layer.maskedCorners = [.layerMinXMaxYCorner,.layerMaxXMaxYCorner]
    }
    
    func makeStartRound(radius: CGFloat? = nil) {
        self.clipsToBounds = true
        self.layer.cornerRadius = radius ?? CGFloat(Constants.Design.ButtonCorderRadius)
        self.layer.maskedCorners = [.layerMinXMaxYCorner,.layerMinXMinYCorner]
    }
    
    func makeEndRound(radius: CGFloat? = nil) {
        self.clipsToBounds = true
        self.layer.cornerRadius = radius ?? CGFloat(Constants.Design.ButtonCorderRadius)
        self.layer.maskedCorners = [.layerMaxXMaxYCorner,.layerMaxXMinYCorner]
    }
    
    func makeStartTopRound() {
        self.clipsToBounds = true
        self.layer.cornerRadius = CGFloat(Constants.Design.ButtonCorderRadius)
        self.layer.maskedCorners = [.layerMinXMinYCorner]
    }
    
    func makeStartTopAndEndBottomRound() {
        self.clipsToBounds = true
        self.layer.cornerRadius = CGFloat(Constants.Design.ButtonCorderRadius)
        self.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMaxYCorner]
    }
    
    func makeEndTopRound() {
        self.clipsToBounds = true
        self.layer.cornerRadius = CGFloat(Constants.Design.ButtonCorderRadius)
        self.layer.maskedCorners = [.layerMaxXMinYCorner]
    }
    
    func makeCircularBorder() {
        self.layer.cornerRadius = self.layer.frame.height / 2
        self.layer.masksToBounds = true
    }
    
    func addShadow(offset: CGSize, color: UIColor, radius: CGFloat, opacity: Float) {
        layer.masksToBounds = false
        layer.shadowOffset = offset
        layer.shadowColor = color.cgColor
        layer.shadowRadius = radius
        layer.shadowOpacity = opacity

        let backgroundCGColor = backgroundColor?.cgColor
        backgroundColor = nil
        layer.backgroundColor =  backgroundCGColor
    }
    
    func drawHorizontalBrokenLines(color: CGColor) {
        self.backgroundColor = UIColor.clear
        
        let shapeLayer = CAShapeLayer()
        shapeLayer.strokeColor = color
        shapeLayer.lineWidth = 1
        shapeLayer.lineDashPattern = [5, 8] // 7 is the length of dash, 3 is length of the gap.
        shapeLayer.lineCap = .round

        let path = CGMutablePath()
        path.addLines(between: [CGPoint(x: self.bounds.minX, y: self.bounds.minY), CGPoint(x: self.bounds.maxX, y: self.bounds.minY)])
        shapeLayer.path = path
        self.layer.addSublayer(shapeLayer)
    }
    func drawHorizontalLines(color: CGColor) {
        self.backgroundColor = UIColor.clear
        
        let shapeLayer = CAShapeLayer()
        shapeLayer.strokeColor = color
        shapeLayer.lineWidth = 1
        shapeLayer.lineCap = .round

        let path = CGMutablePath()
        path.addLines(between: [CGPoint(x: self.bounds.minX, y: self.bounds.minY), CGPoint(x: self.bounds.maxX, y: self.bounds.minY)])
        shapeLayer.path = path
        self.layer.addSublayer(shapeLayer)
    }
    func drawHorizontalLinesmax(color: CGColor) {
        self.backgroundColor = UIColor.clear
        
        let shapeLayer = CAShapeLayer()
        shapeLayer.strokeColor = color
        shapeLayer.lineWidth = 1
        shapeLayer.lineCap = .round

        let path = CGMutablePath()
        path.addLines(between: [CGPoint(x: self.bounds.minX, y: self.bounds.minY), CGPoint(x: self.bounds.maxX, y: self.bounds.maxX)])
        shapeLayer.path = path
        self.layer.addSublayer(shapeLayer)
    }
    func addBottomBorders() {
       let thickness: CGFloat = 1
       let bottomBorder = CALayer()
        bottomBorder.frame = CGRect(x:0, y: self.frame.size.height - thickness, width: self.frame.size.width, height:thickness)
       bottomBorder.backgroundColor = UIColor.black.cgColor
       self.layer.addSublayer(bottomBorder)
    }
    
    
    func addTopBorders() {
       let thickness: CGFloat = 1
       let topBorder = CALayer()
        topBorder.frame = CGRect(x: 0.0, y: 0.0, width: self.frame.size.width, height: thickness)
           topBorder.backgroundColor = UIColor.black.cgColor
       self.layer.addSublayer(topBorder)
    }
    
    func addBottomLine() {
        let border = CALayer()
              let width = CGFloat(1.0)
            border.borderColor = UIColor.black.cgColor
              border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.frame.size.width - 25, height: width)
              border.borderWidth = width
              self.layer.addSublayer(border)
              self.layer.masksToBounds = true
    }
    func drawHorizontalDottedLines(color: CGColor) {
        self.backgroundColor = UIColor.clear
        
        let shapeLayer = CAShapeLayer()
        shapeLayer.strokeColor = color
        shapeLayer.lineWidth = 1
        shapeLayer.lineDashPattern = [1, 5]
        shapeLayer.lineCap = .round

        let path = CGMutablePath()
        path.addLines(between: [CGPoint(x: self.bounds.minX, y: self.bounds.minY), CGPoint(x: self.bounds.maxX, y: self.bounds.minY)])
        shapeLayer.path = path
        self.layer.addSublayer(shapeLayer)
    }
    
    func drawVerticalBrokenLines(color: CGColor) {
        self.backgroundColor = UIColor.clear
        
        let lineLayer = CAShapeLayer()
        lineLayer.strokeColor = color
        lineLayer.lineWidth = 2.5
        lineLayer.lineDashPattern = [1, 6]
        lineLayer.lineCap = .round
        
        let path = CGMutablePath()
        path.addLines(between: [CGPoint(x: self.bounds.minX, y: self.bounds.maxY),
                                CGPoint(x: self.bounds.minX, y: self.bounds.minY)])
        lineLayer.path = path
        self.layer.addSublayer(lineLayer)
    }
    
    func drawVerticalBrokenLines2(color: CGColor) {
        self.backgroundColor = UIColor.clear
        
        let lineLayer = CAShapeLayer()
        lineLayer.strokeColor = color
        lineLayer.lineWidth = 1
        lineLayer.lineDashPattern = [8, 5]
        lineLayer.lineCap = .round
        
        let path = CGMutablePath()
        path.addLines(between: [CGPoint(x: self.bounds.minX, y: self.bounds.maxY),
                                CGPoint(x: self.bounds.minX, y: self.bounds.minY)])
        lineLayer.path = path
        self.layer.addSublayer(lineLayer)
    }
    
    func drawVerticalLines(color: CGColor) {
        self.backgroundColor = UIColor.clear
        
        let lineLayer = CAShapeLayer()
        lineLayer.strokeColor = color
        lineLayer.lineWidth = 1
        lineLayer.lineCap = .round
        
        let path = CGMutablePath()
        path.addLines(between: [CGPoint(x: self.bounds.minX, y: self.bounds.maxY),
                                CGPoint(x: self.bounds.minX, y: self.bounds.minY)])
        lineLayer.path = path
        self.layer.addSublayer(lineLayer)
    }
}
