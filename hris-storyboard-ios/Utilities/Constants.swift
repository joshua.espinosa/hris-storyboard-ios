//
//  Constants.swift
//  hris-storyboard-ios
//
//  Created by Joshua on 1/3/24.
//

import Foundation
import Combine

public class Constants {
    public class Generics {
        public static let Zero = 0
        public static let One = 1
        public static let Two = 2
        public static let Three = 3
        public static let InvalidId = -1
        public static let Space = " "
        public static let SpaceUnicode = "%20"
    }
    
    public class Headers {
        public static let Authorization = "Authorization"
        public static let Bearer = "Bearer "
        public static let Token = "token"
        public static let ContentType = "Content-Type"
        public static let ContentTypeJson = "application/json"
        public static let ContentTypePdf = "application/pdf"
    }
    
    public class Api {
        public static let StatusOk = 200
        public static let ExpiredToken = 401
        public static let GenericError = 500
        public static let InvalidToken = 22
        public static let Http = "http:"
        public static let Https = "https:"
    }
    
    public class Values {
        public static let DispatchAsyncDelay = 0.5
        public static let ErrorMessageDuration = 1.0
        public static let HalfSecond = 0.5
        public static let QuarterSecond = 0.25
        public static let HundredPercent: Float = 100
        public static let BundleIdentifier = "CFBundleVersion"
    }
    
    public class Messages {
        public static let PleaseWait = "Please Wait..."
        public static let NoInternetConnection = "Please check your internet connection and try again."
    }
    
    public class ErrorMessages {
        public static let NoEmail = "Please input your \"Email Address\"."
        public static let InvalidEmail = "The email address is invalid."
        public static let PasswordRequired = "Please input your \"Password\"."
        public static let PasswordRegRequired = "Please input a \"Password\"."
        public static let ConfirmPasswordRequired = "Please input \"Confirm Password\"."
        public static let PasswordNotMatched = "Password does not match."
    }
    
    public class Date {
        public static let ChildBirthdayFormat = "yyyy-MM-dd"
        public static let MonthDayYearFormat = "MM/dd/yyyy"
        public static let YearMonthDayFormat = "yyyy/MM/dd"
        public static let YearMonthDay = "yyyy-MM-dd"
        public static let ApiDateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        public static let GenericDateTimeFormat = "yyyy-MM-dd HH:mm:ss"
        public static let DateTimeFormatWithTimezone = "yyyy-MM-dd'T'HH:mm:ss"
        public static let DateFormatWithWords = "MMM d, yyyy"
        public static let DateFormatWithWordsAndTime = "MMM d, yyyy hh:mm a"
        public static let DateFormatWithWholeMonth = "MMMM d, yyyy"
        public static let MonthAndYear = "MMMM yyyy"
        public static let DayWordMonthWordYear = "EEEE, MMM d, yyyy"
        public static let WholeDayWordMonthWordYear = "EEEE, MMMM d, yyyy"
        public static let Weekend = "yyyy-MM-dd'T'HH:mm:ssZ"
    }
    
    public class Time {
        public static let NormalTimeFormat = "HH:mm:ss"
        public static let TimeWithAMPM = "h:mm a"
        public static let ShortTimeWithAMPM = "h a"
    }
    

    public class Design {
        public static let ButtonCorderRadius = 10
    }
    
    public class Buttons {
        public static let Okay = "OK"
        public static let OkayWord = "OKAY"
        public static let Yes = "Yes"
        public static let No = "No"
        public static let Cancel = "Cancel"
        public static let TryAgain = "Try Again"
        public static let Close = "CLOSE"
        public static let Logout = "Logout"
    }
    
    public class MaxLength {
        public static let Email = 100
        public static let Password = 20
    }
    
    public class Platform {
        public static let Apple = "apple"
        public static let IOS = "ios"
    }
    
    public class DeviceHeight {
        public static let iPhone5s = 568
        public static let iPhone6s = 590
    }
    
}

