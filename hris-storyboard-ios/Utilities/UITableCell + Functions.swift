//
//  UITableCell + Functions.swift
//  hris-storyboard-ios
//
//  Created by Joshua on 1/3/24.
//

import UIKit

extension UITableViewCell {
    func selectColor(color: UIColor) {
        let bgColorView = UIView()
        bgColorView.backgroundColor = color
        self.selectedBackgroundView = bgColorView
    }
}
