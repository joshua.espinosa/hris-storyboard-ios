//
//  DateUtil.swift
//  hris-storyboard-ios
//
//  Created by Joshua on 1/3/24.
//

import Foundation

public class DateUtil {
    public static func getDateFromString(source: String, inputPattern: String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = inputPattern
        guard let date = dateFormatter.date(from: source) else {
            return nil
        }
        return date
    }
    
    public static func getConvertedDate(source: String) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = Constants.Date.GenericDateTimeFormat
        guard let date = dateFormatter.date(from: source) else {
            return nil
        }
        
        dateFormatter.dateFormat = Constants.Date.ChildBirthdayFormat
        return dateFormatter.string(from: date)
    }
    
    public static func converDateFormat(source: String, inputFormat: String, outputFormat: String) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = inputFormat
        guard let date = dateFormatter.date(from: source) else {
            return nil
        }
        
        dateFormatter.dateFormat = outputFormat
        return dateFormatter.string(from: date)
    }
    
    public static func getConvertedDate(source: Date, outputFormat: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = outputFormat
        
        return dateFormatter.string(from: source)
    }
    
    public static func getDateFromIsoFormat(isoSource: String) -> Date? {
        let isoDateFormatter = ISO8601DateFormatter()
        isoDateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        isoDateFormatter.formatOptions = [
            .withFullDate,
            .withFullTime,
            .withDashSeparatorInDate,
            .withFractionalSeconds]
        
        return isoDateFormatter.date(from: isoSource)
    }
    
    public static func getAgeFromDate(source: Date) -> Int? {
        let calendar: NSCalendar! = NSCalendar(calendarIdentifier: .gregorian)
        let now = Date()
        let calcAge = calendar.components(.year, from: source, to: now, options: [])
        let age = calcAge.year
        return age
    }
}
