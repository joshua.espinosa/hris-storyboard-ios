//
//  UIViewController+Functions.swift
//  hris-storyboard-ios
//
//  Created by Joshua on 1/3/24.
//

import Foundation
import UIKit

extension UIViewController {
    
    func showErrorPrompt(message: String, buttonText: String?, action: (() -> ())?) {
        let errorVc = ErrorPromptViewController()
        errorVc.modalPresentationStyle = .custom
        errorVc.message = message
        errorVc.buttonText = buttonText
        errorVc.action = action
        self.present(errorVc, animated: false, completion: nil)
    }
    
    //show pin popup
    func showPin(action: (() -> ())?) {
        let pinVc = EnterPinViewController()
        pinVc.modalPresentationStyle = .custom
        pinVc.action = action
        self.present(pinVc, animated: false, completion: nil)
    }
    
    //show unregistered location popup
    func showUnregisteredPopup(action: (() -> ())?) {
        let pinVc = PendingLocationViewController()
        pinVc.modalPresentationStyle = .custom
        pinVc.action = action
        self.present(pinVc, animated: false, completion: nil)
    }
    
    //show unregistered location popup
    func showPending(action: (() -> ())?) {
        let pinVc = PendingApprovalViewController()
        pinVc.modalPresentationStyle = .custom
        pinVc.action = action
        self.present(pinVc, animated: false, completion: nil)
    }
    
    
    func showSuccessPrompt(headerMessage: String, bodyMessage: String, action: (() -> ())?) {
        let successVc = SuccessPromptViewController()
        successVc.modalPresentationStyle = .custom
        successVc.headerMessage = headerMessage
        successVc.message = bodyMessage
        successVc.action = action
        self.present(successVc, animated: false, completion: nil)
    }
    
    func showErrorPromptNoClose(message: String, buttonText: String?, action: (() -> ())?) {
        let errorVc = ErrorPromptViewController()
        errorVc.modalPresentationStyle = .custom
        errorVc.message = message
        errorVc.buttonText = buttonText
        errorVc.disableButton = true
        errorVc.action = action
        self.present(errorVc, animated: false, completion: nil)
    }
    
}
