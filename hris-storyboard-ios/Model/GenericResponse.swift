//
//  GenericResponse.swift
//  hris-storyboard-ios
//
//  Created by Joshua on 1/17/24.
//

import Foundation

struct GenericResponse: Codable {
    let message: String?
}
