//
//  LeaveListModel.swift
//  hris-storyboard-ios
//
//  Created by Joshua on 1/11/24.
//

import Foundation

struct LeaveListModel: Codable {
    
    let dateFrom: String?
    let dateUntil: String?
    let status: String?
    
}
