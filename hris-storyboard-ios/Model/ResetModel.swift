//
//  ResetModel.swift
//  hris-storyboard-ios
//
//  Created by Gemmalyn Medinaceli on 3/1/24.
//

import Foundation


struct ResetModel: Decodable {
    let token: String?
    let password: String?
    
//    enum CodingKeys: String, CodingKey {
//        case email
//    }
}
