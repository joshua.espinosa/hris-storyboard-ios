//
//  LoginErrorModel.swift
//  hris-storyboard-ios
//
//  Created by Gemmalyn Medinaceli on 2/16/24.
//

import Foundation

// MARK: - LoginErrorModel
struct LoginErrorModel: Decodable {
    let status: String?,
        message: String?,
        data: String?,
        description: String?
}


