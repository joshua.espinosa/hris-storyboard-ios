//
//  LoginDetails.swift
//  hris-storyboard-ios
//
//  Created by Gemmalyn Medinaceli on 2/15/24.
//

import Foundation

// MARK: - ArticleContentModel
struct LoginDetails: Codable {
    let status: Int?
    let message: String?
    let data: LoginClass
}

// MARK: - DataClass
struct LoginClass: Codable {
    let id: Int
    let imageURL, hiredDate: String?
    let token: String?

    enum CodingKeys: String, CodingKey {
        case id
        case imageURL = "image_url"
        case hiredDate = "hired_date"
        case token
    }
}
