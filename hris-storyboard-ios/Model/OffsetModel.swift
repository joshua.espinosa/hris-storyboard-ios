//
//  OffsetModel.swift
//  hris-storyboard-ios
//
//  Created by Joshua on 1/11/24.
//

import Foundation

struct OffsetModel: Codable {
    
    let date: String?
    let time: String?
    let status: String?
    
}
