// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let notificationsModel = try? JSONDecoder().decode(NotificationsModel.self, from: jsonData)

import Foundation

// MARK: - NotificationsModel
struct NotificationsModel: Codable {
    let total, totalPages, currentPage: Int?
    let notifList: [notifLists]?
}

// MARK: - Datum
struct notifLists: Codable {
    let id: Int?
    let date: String?
    let employeeID: Int?
    let fromID, notifyTypeID, leaveID, otID: Int?
    let offsetID: Int?
    let payslipID: Int?
    let holidayID, postAnnouncementID: Int?
    let notifType: String?
    let managerID: Int?
    let managerStatus, adminStatus, employeeStatus, studentInternStatus: String?
    let status, managerReadStatus, adminReadStatus, employeeReadStatus: String?
    let studentInternReadStatus: String?
    let createdBy: Int?
    let createdDate: String?
    let updatedBy: Int?
    let updatedDate: String?

    enum CodingKeys: String, CodingKey {
        case id, date
        case employeeID = "employeeId"
        case fromID = "fromId"
        case notifyTypeID = "notify_typeId"
        case leaveID = "leaveId"
        case otID = "otId"
        case offsetID = "offsetId"
        case payslipID = "payslipId"
        case holidayID = "holiday_id"
        case postAnnouncementID = "post_announcement_id"
        case notifType = "notif_type"
        case managerID = "managerId"
        case managerStatus = "manager_status"
        case adminStatus = "admin_status"
        case employeeStatus = "employee_status"
        case studentInternStatus = "studentIntern_status"
        case status
        case managerReadStatus = "manager_read_status"
        case adminReadStatus = "admin_read_status"
        case employeeReadStatus = "employee_read_status"
        case studentInternReadStatus = "studentIntern_read_status"
        case createdBy = "created_by"
        case createdDate = "created_date"
        case updatedBy = "updated_by"
        case updatedDate = "updated_date"
    }
}

