//
//  ForgotResponse.swift
//  hris-storyboard-ios
//
//  Created by Gemmalyn Medinaceli on 2/27/24.
//

import Foundation

struct ForgotResponse: Decodable {
    let token: String?
    let message: String?
    
    enum CodingKeys: String, CodingKey {
        case message
        case token
    }
    
//    init(token: String) {
//        self.token = token
//    }
}
