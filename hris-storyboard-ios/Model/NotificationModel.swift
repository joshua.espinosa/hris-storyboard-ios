//
//  NotificationModel.swift
//  hris-storyboard-ios
//
//  Created by Joshua on 1/10/24.
//

import Foundation


struct NotificationModel: Codable {
    
    let date: String?
    let type: String?
    
}
