//
//  ErrorModel.swift
//  hris-storyboard-ios
//
//  Created by Joshua on 1/3/24.
//

import Foundation

struct ErrorModel: Codable {
    let code: Int?
    let name: String?
    let errors: [ErrorField]?
}

struct ErrorField: Codable {
    let message, field: String?
}
