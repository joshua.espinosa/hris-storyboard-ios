//
//  TimeHistoryModel.swift
//  hris-storyboard-ios
//
//  Created by Joshua on 1/11/24.
//

import Foundation

struct TimeHistoryModel: Codable {
    
    let date: String?
    let timeIn: String?
    let timeOut: String
    let status: String?
    
}
