//
//  ProfileModel.swift
//  hris-storyboard-ios
//
//  Created by Gemmalyn Medinaceli on 3/6/24.
//

import Foundation

struct ProfileModel: Codable {
    let status: Int?
    let message: String?
    let data: profileLists?
    
    
}

// MARK: - DataClass
struct profileLists: Codable {
    let id, staffNumber: Int?
    let firstname, middlename, lastname: String?
    let suffix: String?
    let name, gender, dob, mobileNumber: String?
    let civilStatus, address, sss, philhealth: String?
    let pagIbig, taxID, userRole, designation: String?
    let employmentType: String?
    let hiredDate: String?
    let status: String?
    let resignedDate, separationDate: String?
    let department, asLOC, regIP: String?
    let salary: Int?
    let username, emailAddress, password, resetTokenExpiration: String?
    let resetToken, contactPersonName, contactPersonAddress, contactPersonNumber: String?
    let contactPersonEmail: String?
    let contactPersonRelationship: String?
    let age, nationality: String?
    let statusAccount, statusDisabled, statusVerification, statusDisabledExpiry: String
    let passwordResetURL, passwordResetCode: String?
    let passwordAttemptCount: Int?
    let imageURL, imageURLID, createdBy: String?
    let createdDate: String?
    let updatedBy: String?
    let updatedDate: String?

    enum CodingKeys: String, CodingKey {
        case id
        case staffNumber = "staff_number"
        case firstname, middlename, lastname, suffix, name, gender, dob
        case mobileNumber = "mobile_number"
        case civilStatus = "civil_status"
        case address, sss, philhealth
        case pagIbig = "pag_ibig"
        case taxID = "tax_id"
        case userRole = "user_role"
        case designation
        case employmentType = "employment_type"
        case hiredDate = "hired_date"
        case status
        case resignedDate = "resigned_date"
        case separationDate = "separation_date"
        case department
        case asLOC = "as_loc"
        case regIP = "reg_ip"
        case salary, username
        case emailAddress = "email_address"
        case password
        case resetTokenExpiration = "reset_token_expiration"
        case resetToken = "reset_token"
        case contactPersonName = "contact_person_name"
        case contactPersonAddress = "contact_person_address"
        case contactPersonNumber = "contact_person_number"
        case contactPersonEmail = "contact_person_email"
        case contactPersonRelationship = "contact_person_relationship"
        case age, nationality
        case statusAccount = "status_account"
        case statusDisabled = "status_disabled"
        case statusVerification = "status_verification"
        case statusDisabledExpiry = "status_disabled_expiry"
        case passwordResetURL = "password_reset_url"
        case passwordResetCode = "password_reset_code"
        case passwordAttemptCount = "password_attempt_count"
        case imageURL = "image_url"
        case imageURLID = "image_url_id"
        case createdBy = "created_by"
        case createdDate = "created_date"
        case updatedBy = "updated_by"
        case updatedDate = "updated_date"
    }
}
