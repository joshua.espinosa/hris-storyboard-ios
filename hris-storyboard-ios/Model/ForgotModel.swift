//
//  ForgotModel.swift
//  hris-storyboard-ios
//
//  Created by Gemmalyn Medinaceli on 2/26/24.
//

import Foundation

struct ForgotModel: Decodable {
    let email: String?
    
//    enum CodingKeys: String, CodingKey {
//        case email
//    }
}
